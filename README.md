# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* TRIVIAL ONLINE "EN CONSTRUCCION"
* Version World Of Warcraft v.1.0.5

### How do I get set up? ###

* Configuracion de MySql
* Ejecutar el trivial.sql
* Configurar el parameters.yml dentro de /app/config
* Database configuration
* run server con php bin/console server:start "IPSERVER"
* Connect to http://127.0.0.1:8000/

### Contribution guidelines ###

# PHP #
* Control de acceso solo a usuarios registrados
* Control de partidas, con creación de nueva partida en caso de no haber ninguna abierta
* Eliminación de partida
* Creación de partida
* Acceso a partida por id departida creada
* Establecer acceso a partida solo en caso de que la partida este en estado "open"

# JAVASCRIPT #
* Las fichas no se mueven hasta que tiras el dado
* Se moverán en el orden establecido siempre que cambie el turno al lanzar una nueva tirada de dados
* Las fichas solo se mueven por las casillas habilitadas a tal efecto