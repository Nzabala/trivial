<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_12f738d54aff3bf3f6a56dc81f2505fdcba5d762d3d028c6a50f1a44ad74da5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb235111c21ea60dc736636c3d9eae3af3f280e1fea38ef1593434857910e60b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb235111c21ea60dc736636c3d9eae3af3f280e1fea38ef1593434857910e60b->enter($__internal_fb235111c21ea60dc736636c3d9eae3af3f280e1fea38ef1593434857910e60b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_4e229c5d613d48baa4156af1ae241c804de617cbb2f16fe09f2c8611639576b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e229c5d613d48baa4156af1ae241c804de617cbb2f16fe09f2c8611639576b2->enter($__internal_4e229c5d613d48baa4156af1ae241c804de617cbb2f16fe09f2c8611639576b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fb235111c21ea60dc736636c3d9eae3af3f280e1fea38ef1593434857910e60b->leave($__internal_fb235111c21ea60dc736636c3d9eae3af3f280e1fea38ef1593434857910e60b_prof);

        
        $__internal_4e229c5d613d48baa4156af1ae241c804de617cbb2f16fe09f2c8611639576b2->leave($__internal_4e229c5d613d48baa4156af1ae241c804de617cbb2f16fe09f2c8611639576b2_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b91cd99dc31ec8b010fabd99d90f05baf54d8a970e924932d32198661b26518c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b91cd99dc31ec8b010fabd99d90f05baf54d8a970e924932d32198661b26518c->enter($__internal_b91cd99dc31ec8b010fabd99d90f05baf54d8a970e924932d32198661b26518c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7d9c60592e76a317d83e000fe438bfefda4361d4e8994cac98b8fda13379f931 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d9c60592e76a317d83e000fe438bfefda4361d4e8994cac98b8fda13379f931->enter($__internal_7d9c60592e76a317d83e000fe438bfefda4361d4e8994cac98b8fda13379f931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_7d9c60592e76a317d83e000fe438bfefda4361d4e8994cac98b8fda13379f931->leave($__internal_7d9c60592e76a317d83e000fe438bfefda4361d4e8994cac98b8fda13379f931_prof);

        
        $__internal_b91cd99dc31ec8b010fabd99d90f05baf54d8a970e924932d32198661b26518c->leave($__internal_b91cd99dc31ec8b010fabd99d90f05baf54d8a970e924932d32198661b26518c_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9f8a8ae93d5817d823c9867e4fd4baec76c6211fdd6b7334e39fcbee1c9cc018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f8a8ae93d5817d823c9867e4fd4baec76c6211fdd6b7334e39fcbee1c9cc018->enter($__internal_9f8a8ae93d5817d823c9867e4fd4baec76c6211fdd6b7334e39fcbee1c9cc018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_c170a877dce38687f24a58d924259ce815c4acbc7fc557b33c325f9b95043f57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c170a877dce38687f24a58d924259ce815c4acbc7fc557b33c325f9b95043f57->enter($__internal_c170a877dce38687f24a58d924259ce815c4acbc7fc557b33c325f9b95043f57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_c170a877dce38687f24a58d924259ce815c4acbc7fc557b33c325f9b95043f57->leave($__internal_c170a877dce38687f24a58d924259ce815c4acbc7fc557b33c325f9b95043f57_prof);

        
        $__internal_9f8a8ae93d5817d823c9867e4fd4baec76c6211fdd6b7334e39fcbee1c9cc018->leave($__internal_9f8a8ae93d5817d823c9867e4fd4baec76c6211fdd6b7334e39fcbee1c9cc018_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_37ba687bd4adeb7b6c6b7c4ba7495a9808b21613d39b3fca771152defe26fdc8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37ba687bd4adeb7b6c6b7c4ba7495a9808b21613d39b3fca771152defe26fdc8->enter($__internal_37ba687bd4adeb7b6c6b7c4ba7495a9808b21613d39b3fca771152defe26fdc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f5e2e9c0ef6930e5d61e5e43034d92e15c78eb6cf57ab6a153b8b04e50c6855e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5e2e9c0ef6930e5d61e5e43034d92e15c78eb6cf57ab6a153b8b04e50c6855e->enter($__internal_f5e2e9c0ef6930e5d61e5e43034d92e15c78eb6cf57ab6a153b8b04e50c6855e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_f5e2e9c0ef6930e5d61e5e43034d92e15c78eb6cf57ab6a153b8b04e50c6855e->leave($__internal_f5e2e9c0ef6930e5d61e5e43034d92e15c78eb6cf57ab6a153b8b04e50c6855e_prof);

        
        $__internal_37ba687bd4adeb7b6c6b7c4ba7495a9808b21613d39b3fca771152defe26fdc8->leave($__internal_37ba687bd4adeb7b6c6b7c4ba7495a9808b21613d39b3fca771152defe26fdc8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
