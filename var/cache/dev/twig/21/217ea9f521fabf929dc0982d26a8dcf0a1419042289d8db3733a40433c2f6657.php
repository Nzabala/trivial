<?php

/* message.html.twig */
class __TwigTemplate_54d9d3861155b980f5bea54ef95db194bbe998c1ab9579e6e316f7c9c29f2664 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "message.html.twig", 2);
        $this->blocks = array(
            'message' => array($this, 'block_message'),
            'message2' => array($this, 'block_message2'),
            'buttonLogin' => array($this, 'block_buttonLogin'),
            'button' => array($this, 'block_button'),
            'backButton' => array($this, 'block_backButton'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_351344a80de0f681a40820f13ba17b2b857ffdc58cfc7be0e12a34c320885c02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_351344a80de0f681a40820f13ba17b2b857ffdc58cfc7be0e12a34c320885c02->enter($__internal_351344a80de0f681a40820f13ba17b2b857ffdc58cfc7be0e12a34c320885c02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message.html.twig"));

        $__internal_f74ca0d09366675e2858c365ebcbc35d5d721ce7c0d0f00a720a53be3c2fd0d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f74ca0d09366675e2858c365ebcbc35d5d721ce7c0d0f00a720a53be3c2fd0d9->enter($__internal_f74ca0d09366675e2858c365ebcbc35d5d721ce7c0d0f00a720a53be3c2fd0d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_351344a80de0f681a40820f13ba17b2b857ffdc58cfc7be0e12a34c320885c02->leave($__internal_351344a80de0f681a40820f13ba17b2b857ffdc58cfc7be0e12a34c320885c02_prof);

        
        $__internal_f74ca0d09366675e2858c365ebcbc35d5d721ce7c0d0f00a720a53be3c2fd0d9->leave($__internal_f74ca0d09366675e2858c365ebcbc35d5d721ce7c0d0f00a720a53be3c2fd0d9_prof);

    }

    // line 3
    public function block_message($context, array $blocks = array())
    {
        $__internal_a6cf65298400d16fff0c7ad7fa90463886931d604c2b03291bb0eb5e3d74b78f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6cf65298400d16fff0c7ad7fa90463886931d604c2b03291bb0eb5e3d74b78f->enter($__internal_a6cf65298400d16fff0c7ad7fa90463886931d604c2b03291bb0eb5e3d74b78f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message"));

        $__internal_a80da3e605f78d1bdd4301b853c615d72c8abe85f2d3c703dfa3a88e73402233 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a80da3e605f78d1bdd4301b853c615d72c8abe85f2d3c703dfa3a88e73402233->enter($__internal_a80da3e605f78d1bdd4301b853c615d72c8abe85f2d3c703dfa3a88e73402233_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message"));

        // line 4
        echo "<div class=\"message\">
    ";
        // line 5
        echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
        echo "
</div> 
";
        
        $__internal_a80da3e605f78d1bdd4301b853c615d72c8abe85f2d3c703dfa3a88e73402233->leave($__internal_a80da3e605f78d1bdd4301b853c615d72c8abe85f2d3c703dfa3a88e73402233_prof);

        
        $__internal_a6cf65298400d16fff0c7ad7fa90463886931d604c2b03291bb0eb5e3d74b78f->leave($__internal_a6cf65298400d16fff0c7ad7fa90463886931d604c2b03291bb0eb5e3d74b78f_prof);

    }

    // line 8
    public function block_message2($context, array $blocks = array())
    {
        $__internal_714e049e1393c147e5ebff549b6d4a7b49ec40db10591d5fb908121bc9c8aa86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_714e049e1393c147e5ebff549b6d4a7b49ec40db10591d5fb908121bc9c8aa86->enter($__internal_714e049e1393c147e5ebff549b6d4a7b49ec40db10591d5fb908121bc9c8aa86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message2"));

        $__internal_b210014b10b26df8260581bea98b25adc349ef7cb094d0b87edee3d1e52629fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b210014b10b26df8260581bea98b25adc349ef7cb094d0b87edee3d1e52629fc->enter($__internal_b210014b10b26df8260581bea98b25adc349ef7cb094d0b87edee3d1e52629fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message2"));

        // line 9
        echo "<div class=\"message2\">
    ";
        // line 10
        echo twig_escape_filter($this->env, ($context["message2"] ?? $this->getContext($context, "message2")), "html", null, true);
        echo "
</div> 
";
        
        $__internal_b210014b10b26df8260581bea98b25adc349ef7cb094d0b87edee3d1e52629fc->leave($__internal_b210014b10b26df8260581bea98b25adc349ef7cb094d0b87edee3d1e52629fc_prof);

        
        $__internal_714e049e1393c147e5ebff549b6d4a7b49ec40db10591d5fb908121bc9c8aa86->leave($__internal_714e049e1393c147e5ebff549b6d4a7b49ec40db10591d5fb908121bc9c8aa86_prof);

    }

    // line 13
    public function block_buttonLogin($context, array $blocks = array())
    {
        $__internal_7497ee431a5d9d314288f42c95f8e935f229267f6a4f899a2ea41c95389a2d62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7497ee431a5d9d314288f42c95f8e935f229267f6a4f899a2ea41c95389a2d62->enter($__internal_7497ee431a5d9d314288f42c95f8e935f229267f6a4f899a2ea41c95389a2d62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        $__internal_e4b59863a9d42b0e657563ab1950c6447aec1f1e66f3802c22a28292a19428e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4b59863a9d42b0e657563ab1950c6447aec1f1e66f3802c22a28292a19428e9->enter($__internal_e4b59863a9d42b0e657563ab1950c6447aec1f1e66f3802c22a28292a19428e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        
        $__internal_e4b59863a9d42b0e657563ab1950c6447aec1f1e66f3802c22a28292a19428e9->leave($__internal_e4b59863a9d42b0e657563ab1950c6447aec1f1e66f3802c22a28292a19428e9_prof);

        
        $__internal_7497ee431a5d9d314288f42c95f8e935f229267f6a4f899a2ea41c95389a2d62->leave($__internal_7497ee431a5d9d314288f42c95f8e935f229267f6a4f899a2ea41c95389a2d62_prof);

    }

    // line 14
    public function block_button($context, array $blocks = array())
    {
        $__internal_4a113dd83028c0676af2bc315deb329f6a88efba2fe1725657a9762136cb4d91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a113dd83028c0676af2bc315deb329f6a88efba2fe1725657a9762136cb4d91->enter($__internal_4a113dd83028c0676af2bc315deb329f6a88efba2fe1725657a9762136cb4d91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button"));

        $__internal_8d1ac283ce4c11dde583f226d4d231d0f05b486dab8cec6c06e112575d5a62bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d1ac283ce4c11dde583f226d4d231d0f05b486dab8cec6c06e112575d5a62bb->enter($__internal_8d1ac283ce4c11dde583f226d4d231d0f05b486dab8cec6c06e112575d5a62bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button"));

        // line 15
        echo "
";
        // line 16
        if ((($context["message"] ?? $this->getContext($context, "message")) == "No hay partidas")) {
            // line 17
            echo "<div class=\"message2\">  
\t\t<button type=\"button\" onclick=\"window.location.href='/newGame'\">Crear Partida</button>
</div>
";
        }
        // line 21
        if ((($context["message"] ?? $this->getContext($context, "message")) == "Partida creada correctamente!")) {
            // line 22
            echo "<div class=\"message2\">  
\t\t<button type=\"button\" onclick=\"window.location.href='/partida/";
            // line 23
            echo twig_escape_filter($this->env, ($context["idPartida"] ?? $this->getContext($context, "idPartida")), "html", null, true);
            echo "'\">Ir a la Partida</button>
</div>
";
        }
        
        $__internal_8d1ac283ce4c11dde583f226d4d231d0f05b486dab8cec6c06e112575d5a62bb->leave($__internal_8d1ac283ce4c11dde583f226d4d231d0f05b486dab8cec6c06e112575d5a62bb_prof);

        
        $__internal_4a113dd83028c0676af2bc315deb329f6a88efba2fe1725657a9762136cb4d91->leave($__internal_4a113dd83028c0676af2bc315deb329f6a88efba2fe1725657a9762136cb4d91_prof);

    }

    // line 27
    public function block_backButton($context, array $blocks = array())
    {
        $__internal_fc09403ca8068563d40c7d003c1160c1a8499df505f1604d6e8b79a6f0522304 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc09403ca8068563d40c7d003c1160c1a8499df505f1604d6e8b79a6f0522304->enter($__internal_fc09403ca8068563d40c7d003c1160c1a8499df505f1604d6e8b79a6f0522304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        $__internal_ecfade22d8b3b6f571c5f43e90ea0a0e86c2614f4bcd71d70afe690eac58f64f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ecfade22d8b3b6f571c5f43e90ea0a0e86c2614f4bcd71d70afe690eac58f64f->enter($__internal_ecfade22d8b3b6f571c5f43e90ea0a0e86c2614f4bcd71d70afe690eac58f64f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        // line 28
        echo "    <button class=\"backButton\" type=\"button\" onCLick=\"history.back()\">Atras</button>
";
        
        $__internal_ecfade22d8b3b6f571c5f43e90ea0a0e86c2614f4bcd71d70afe690eac58f64f->leave($__internal_ecfade22d8b3b6f571c5f43e90ea0a0e86c2614f4bcd71d70afe690eac58f64f_prof);

        
        $__internal_fc09403ca8068563d40c7d003c1160c1a8499df505f1604d6e8b79a6f0522304->leave($__internal_fc09403ca8068563d40c7d003c1160c1a8499df505f1604d6e8b79a6f0522304_prof);

    }

    public function getTemplateName()
    {
        return "message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 28,  150 => 27,  136 => 23,  133 => 22,  131 => 21,  125 => 17,  123 => 16,  120 => 15,  111 => 14,  94 => 13,  81 => 10,  78 => 9,  69 => 8,  56 => 5,  53 => 4,  44 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/message.html.twig #}
{% extends 'base.html.twig' %}
{% block message %}
<div class=\"message\">
    {{ message }}
</div> 
{% endblock %}
{% block message2 %}
<div class=\"message2\">
    {{ message2 }}
</div> 
{% endblock %}
{% block buttonLogin %}{% endblock %}
{% block button %}

{% if message == \"No hay partidas\" %}
<div class=\"message2\">  
\t\t<button type=\"button\" onclick=\"window.location.href='/newGame'\">Crear Partida</button>
</div>
{% endif %}
{% if message == \"Partida creada correctamente!\" %}
<div class=\"message2\">  
\t\t<button type=\"button\" onclick=\"window.location.href='/partida/{{idPartida}}'\">Ir a la Partida</button>
</div>
{% endif %}
{% endblock %}
{% block backButton %}
    <button class=\"backButton\" type=\"button\" onCLick=\"history.back()\">Atras</button>
{% endblock %}

", "message.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/message.html.twig");
    }
}
