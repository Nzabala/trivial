<?php

/* jugadores/content.html.twig */
class __TwigTemplate_dd01c9486297d26b55569cfcee154f2961ea46a1237a641e978b84abfe9e4b38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "jugadores/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c07911f855a0a6f92394a41dadbcf9996c5cd923a2b269b6aa8214960eed675 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c07911f855a0a6f92394a41dadbcf9996c5cd923a2b269b6aa8214960eed675->enter($__internal_3c07911f855a0a6f92394a41dadbcf9996c5cd923a2b269b6aa8214960eed675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "jugadores/content.html.twig"));

        $__internal_64eb5d695edad3f6d8836f334fdfae6450fa78af4bbc08bb4e06212809869351 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64eb5d695edad3f6d8836f334fdfae6450fa78af4bbc08bb4e06212809869351->enter($__internal_64eb5d695edad3f6d8836f334fdfae6450fa78af4bbc08bb4e06212809869351_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "jugadores/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c07911f855a0a6f92394a41dadbcf9996c5cd923a2b269b6aa8214960eed675->leave($__internal_3c07911f855a0a6f92394a41dadbcf9996c5cd923a2b269b6aa8214960eed675_prof);

        
        $__internal_64eb5d695edad3f6d8836f334fdfae6450fa78af4bbc08bb4e06212809869351->leave($__internal_64eb5d695edad3f6d8836f334fdfae6450fa78af4bbc08bb4e06212809869351_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_725a1d0adf48dd77b184bff35e4d847c0282aaa954a3310ed0c765441be5f2d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_725a1d0adf48dd77b184bff35e4d847c0282aaa954a3310ed0c765441be5f2d0->enter($__internal_725a1d0adf48dd77b184bff35e4d847c0282aaa954a3310ed0c765441be5f2d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_263a67b1b6959e57604850ab76a8457e112dc26c0dbab91998c72f3c1d65e284 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_263a67b1b6959e57604850ab76a8457e112dc26c0dbab91998c72f3c1d65e284->enter($__internal_263a67b1b6959e57604850ab76a8457e112dc26c0dbab91998c72f3c1d65e284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> Lista de jugadores </h3>
<table border=\"1\">
    <tr>
        <th>id</th>
        <th>nombre</th>
        <th>puntos</th>
\t\t<th>victorias</th>
    </tr>
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["jugadores"]);
        foreach ($context['_seq'] as $context["_key"] => $context["jugadores"]) {
            // line 13
            echo "        <tr>
            <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["jugadores"], "playerid", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["jugadores"], "name", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["jugadores"], "score", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["jugadores"], "wins", array()), "html", null, true);
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['jugadores'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "</table>
";
        
        $__internal_263a67b1b6959e57604850ab76a8457e112dc26c0dbab91998c72f3c1d65e284->leave($__internal_263a67b1b6959e57604850ab76a8457e112dc26c0dbab91998c72f3c1d65e284_prof);

        
        $__internal_725a1d0adf48dd77b184bff35e4d847c0282aaa954a3310ed0c765441be5f2d0->leave($__internal_725a1d0adf48dd77b184bff35e4d847c0282aaa954a3310ed0c765441be5f2d0_prof);

    }

    public function getTemplateName()
    {
        return "jugadores/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 20,  78 => 17,  74 => 16,  70 => 15,  66 => 14,  63 => 13,  59 => 12,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/jugadores/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <h3> Lista de jugadores </h3>
<table border=\"1\">
    <tr>
        <th>id</th>
        <th>nombre</th>
        <th>puntos</th>
\t\t<th>victorias</th>
    </tr>
    {% for jugadores in jugadores %}
        <tr>
            <td>{{ jugadores.playerid }}</td>
            <td>{{ jugadores.name }}</td>
            <td>{{ jugadores.score }}</td>
            <td>{{ jugadores.wins }}</td>
        </tr>
    {% endfor %}
</table>
{% endblock %}", "jugadores/content.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/jugadores/content.html.twig");
    }
}
