<?php

/* partidas/form.html.twig */
class __TwigTemplate_73687d2cf23b9b03b03479786c4336b4f11fca0e5528b42ef8f8851e4abfc4b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "partidas/form.html.twig", 2);
        $this->blocks = array(
            'form' => array($this, 'block_form'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_505edc8554c95611d36d60729019e2457604c64cfb2ba1c2af2a9b0d0912d919 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_505edc8554c95611d36d60729019e2457604c64cfb2ba1c2af2a9b0d0912d919->enter($__internal_505edc8554c95611d36d60729019e2457604c64cfb2ba1c2af2a9b0d0912d919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partidas/form.html.twig"));

        $__internal_64d76331432b04102a117450b20e0682eb738aad78b3bc654508ec999b85c20a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64d76331432b04102a117450b20e0682eb738aad78b3bc654508ec999b85c20a->enter($__internal_64d76331432b04102a117450b20e0682eb738aad78b3bc654508ec999b85c20a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partidas/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_505edc8554c95611d36d60729019e2457604c64cfb2ba1c2af2a9b0d0912d919->leave($__internal_505edc8554c95611d36d60729019e2457604c64cfb2ba1c2af2a9b0d0912d919_prof);

        
        $__internal_64d76331432b04102a117450b20e0682eb738aad78b3bc654508ec999b85c20a->leave($__internal_64d76331432b04102a117450b20e0682eb738aad78b3bc654508ec999b85c20a_prof);

    }

    // line 3
    public function block_form($context, array $blocks = array())
    {
        $__internal_3f3f5a92baca2526c0239fe1ead44904f9aaecc8d5c9e8dde503bbb8810b80bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f3f5a92baca2526c0239fe1ead44904f9aaecc8d5c9e8dde503bbb8810b80bc->enter($__internal_3f3f5a92baca2526c0239fe1ead44904f9aaecc8d5c9e8dde503bbb8810b80bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_a6c97b05115b9515b5fdfbca8bdfaa76e4c1fa8af76557102fca16c2b1508962 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6c97b05115b9515b5fdfbca8bdfaa76e4c1fa8af76557102fca16c2b1508962->enter($__internal_a6c97b05115b9515b5fdfbca8bdfaa76e4c1fa8af76557102fca16c2b1508962_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 4
        echo "\t<h3> ";
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "

    <div class=\"login-form\">  
\t  <div class=\"field\">
\t  ";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo " 
\t</div>
\t";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

";
        
        $__internal_a6c97b05115b9515b5fdfbca8bdfaa76e4c1fa8af76557102fca16c2b1508962->leave($__internal_a6c97b05115b9515b5fdfbca8bdfaa76e4c1fa8af76557102fca16c2b1508962_prof);

        
        $__internal_3f3f5a92baca2526c0239fe1ead44904f9aaecc8d5c9e8dde503bbb8810b80bc->leave($__internal_3f3f5a92baca2526c0239fe1ead44904f9aaecc8d5c9e8dde503bbb8810b80bc_prof);

    }

    public function getTemplateName()
    {
        return "partidas/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 11,  61 => 9,  54 => 5,  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/partidas/form.html.twig #}
{% extends 'base.html.twig' %}
{% block form %}
\t<h3> {{title}} </h3>
    {{ form_start(form) }}

    <div class=\"login-form\">  
\t  <div class=\"field\">
\t  {{ form_widget(form) }} 
\t</div>
\t{{ form_end(form) }}

{% endblock %}", "partidas/form.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/partidas/form.html.twig");
    }
}
