<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_490088ef8c2d536ba9aa3e4da532e3c9d5d6875dde7bb492995d7d77f4e311f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26843cc28f5b3ed9853aa1ff65a924bc01397d57911fd5c46c3328b99a2c8108 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26843cc28f5b3ed9853aa1ff65a924bc01397d57911fd5c46c3328b99a2c8108->enter($__internal_26843cc28f5b3ed9853aa1ff65a924bc01397d57911fd5c46c3328b99a2c8108_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_ba50a6ccf0689d25a8543f5f1e7ff3fe3f1ed8851a954f3ef1fc2603e695a23d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba50a6ccf0689d25a8543f5f1e7ff3fe3f1ed8851a954f3ef1fc2603e695a23d->enter($__internal_ba50a6ccf0689d25a8543f5f1e7ff3fe3f1ed8851a954f3ef1fc2603e695a23d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_26843cc28f5b3ed9853aa1ff65a924bc01397d57911fd5c46c3328b99a2c8108->leave($__internal_26843cc28f5b3ed9853aa1ff65a924bc01397d57911fd5c46c3328b99a2c8108_prof);

        
        $__internal_ba50a6ccf0689d25a8543f5f1e7ff3fe3f1ed8851a954f3ef1fc2603e695a23d->leave($__internal_ba50a6ccf0689d25a8543f5f1e7ff3fe3f1ed8851a954f3ef1fc2603e695a23d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_8df1cef231d42c6c0f8b4a7fec846e983e007159fbe892fc4b788767f593db52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8df1cef231d42c6c0f8b4a7fec846e983e007159fbe892fc4b788767f593db52->enter($__internal_8df1cef231d42c6c0f8b4a7fec846e983e007159fbe892fc4b788767f593db52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_f4e08830c55cc998f77d83a643281933059fdb6752a121b9a60d46fe18e98562 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4e08830c55cc998f77d83a643281933059fdb6752a121b9a60d46fe18e98562->enter($__internal_f4e08830c55cc998f77d83a643281933059fdb6752a121b9a60d46fe18e98562_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f4e08830c55cc998f77d83a643281933059fdb6752a121b9a60d46fe18e98562->leave($__internal_f4e08830c55cc998f77d83a643281933059fdb6752a121b9a60d46fe18e98562_prof);

        
        $__internal_8df1cef231d42c6c0f8b4a7fec846e983e007159fbe892fc4b788767f593db52->leave($__internal_8df1cef231d42c6c0f8b4a7fec846e983e007159fbe892fc4b788767f593db52_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ffa4a0e139eae205ec2b1e8ec96d6f490ac5bd8d34e6e70fa4017e625291f677 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffa4a0e139eae205ec2b1e8ec96d6f490ac5bd8d34e6e70fa4017e625291f677->enter($__internal_ffa4a0e139eae205ec2b1e8ec96d6f490ac5bd8d34e6e70fa4017e625291f677_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_64c261febc2f01ebdb98ee8aead422f11ec89a4f461eb3d23567b6b57af836ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64c261febc2f01ebdb98ee8aead422f11ec89a4f461eb3d23567b6b57af836ed->enter($__internal_64c261febc2f01ebdb98ee8aead422f11ec89a4f461eb3d23567b6b57af836ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_64c261febc2f01ebdb98ee8aead422f11ec89a4f461eb3d23567b6b57af836ed->leave($__internal_64c261febc2f01ebdb98ee8aead422f11ec89a4f461eb3d23567b6b57af836ed_prof);

        
        $__internal_ffa4a0e139eae205ec2b1e8ec96d6f490ac5bd8d34e6e70fa4017e625291f677->leave($__internal_ffa4a0e139eae205ec2b1e8ec96d6f490ac5bd8d34e6e70fa4017e625291f677_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_4d2144ec76b654e9431c6071b8b51875a48bf9208cc65e29bbbdda70e7aad3d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d2144ec76b654e9431c6071b8b51875a48bf9208cc65e29bbbdda70e7aad3d8->enter($__internal_4d2144ec76b654e9431c6071b8b51875a48bf9208cc65e29bbbdda70e7aad3d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_fa0d8c9aa9232e41ee43abe851d94326f2fdb9baf8c06dad09dc115984fb6572 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa0d8c9aa9232e41ee43abe851d94326f2fdb9baf8c06dad09dc115984fb6572->enter($__internal_fa0d8c9aa9232e41ee43abe851d94326f2fdb9baf8c06dad09dc115984fb6572_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_fa0d8c9aa9232e41ee43abe851d94326f2fdb9baf8c06dad09dc115984fb6572->leave($__internal_fa0d8c9aa9232e41ee43abe851d94326f2fdb9baf8c06dad09dc115984fb6572_prof);

        
        $__internal_4d2144ec76b654e9431c6071b8b51875a48bf9208cc65e29bbbdda70e7aad3d8->leave($__internal_4d2144ec76b654e9431c6071b8b51875a48bf9208cc65e29bbbdda70e7aad3d8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
