<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f902452bd4e2b5bd61051f47431cb025fc906d0603e67cac5f83a0eb7cfdef60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bc5997521f7fe34146d4756f9b5302818730671d97f62fa86a2d3be09595520 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bc5997521f7fe34146d4756f9b5302818730671d97f62fa86a2d3be09595520->enter($__internal_5bc5997521f7fe34146d4756f9b5302818730671d97f62fa86a2d3be09595520_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_8e331d6dae2546f483a27cabd9e36e272967b9bbe1049a14afee2cf4e42ddbe0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e331d6dae2546f483a27cabd9e36e272967b9bbe1049a14afee2cf4e42ddbe0->enter($__internal_8e331d6dae2546f483a27cabd9e36e272967b9bbe1049a14afee2cf4e42ddbe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5bc5997521f7fe34146d4756f9b5302818730671d97f62fa86a2d3be09595520->leave($__internal_5bc5997521f7fe34146d4756f9b5302818730671d97f62fa86a2d3be09595520_prof);

        
        $__internal_8e331d6dae2546f483a27cabd9e36e272967b9bbe1049a14afee2cf4e42ddbe0->leave($__internal_8e331d6dae2546f483a27cabd9e36e272967b9bbe1049a14afee2cf4e42ddbe0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_30adeb22ac455209812dac1712a59265da36f5165341fc49522a52e0f54006e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30adeb22ac455209812dac1712a59265da36f5165341fc49522a52e0f54006e2->enter($__internal_30adeb22ac455209812dac1712a59265da36f5165341fc49522a52e0f54006e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_7c1c4f5b07dfa00c5fac24dc19550c0869c80ca96dd7cc52790e087ad6896f73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c1c4f5b07dfa00c5fac24dc19550c0869c80ca96dd7cc52790e087ad6896f73->enter($__internal_7c1c4f5b07dfa00c5fac24dc19550c0869c80ca96dd7cc52790e087ad6896f73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_7c1c4f5b07dfa00c5fac24dc19550c0869c80ca96dd7cc52790e087ad6896f73->leave($__internal_7c1c4f5b07dfa00c5fac24dc19550c0869c80ca96dd7cc52790e087ad6896f73_prof);

        
        $__internal_30adeb22ac455209812dac1712a59265da36f5165341fc49522a52e0f54006e2->leave($__internal_30adeb22ac455209812dac1712a59265da36f5165341fc49522a52e0f54006e2_prof);

    }

    // line 6
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8c72123b7ae6a56b6b25a0a589bd6607058b840c386042894c24e946245f7c5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c72123b7ae6a56b6b25a0a589bd6607058b840c386042894c24e946245f7c5d->enter($__internal_8c72123b7ae6a56b6b25a0a589bd6607058b840c386042894c24e946245f7c5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_37f1c6f315071ef320d6a50492d54d9712d0c6e265f17f4baa0845ac19fa5e79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37f1c6f315071ef320d6a50492d54d9712d0c6e265f17f4baa0845ac19fa5e79->enter($__internal_37f1c6f315071ef320d6a50492d54d9712d0c6e265f17f4baa0845ac19fa5e79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_37f1c6f315071ef320d6a50492d54d9712d0c6e265f17f4baa0845ac19fa5e79->leave($__internal_37f1c6f315071ef320d6a50492d54d9712d0c6e265f17f4baa0845ac19fa5e79_prof);

        
        $__internal_8c72123b7ae6a56b6b25a0a589bd6607058b840c386042894c24e946245f7c5d->leave($__internal_8c72123b7ae6a56b6b25a0a589bd6607058b840c386042894c24e946245f7c5d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 6,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
{% block javascripts %}{% endblock %}", "@FOSUser/Security/login.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
