<?php

/* tablero.html.twig */
class __TwigTemplate_93d8bca36f956a3ae05540f2ce2b25ba6df76d54e9e5fd37f142b9192222c17c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fac708cfa1e71ef0e4e092cabbf01d9737b268b63c1d86854856811e469c79a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fac708cfa1e71ef0e4e092cabbf01d9737b268b63c1d86854856811e469c79a5->enter($__internal_fac708cfa1e71ef0e4e092cabbf01d9737b268b63c1d86854856811e469c79a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "tablero.html.twig"));

        $__internal_52e9729b5f1f4204d9f63a56d5164f82037088d66dd1e09804ba56126d3646e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52e9729b5f1f4204d9f63a56d5164f82037088d66dd1e09804ba56126d3646e1->enter($__internal_52e9729b5f1f4204d9f63a56d5164f82037088d66dd1e09804ba56126d3646e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "tablero.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
<style>
.casilla {
\tposition: absolute;
\tfloat: left;
\twidth: 25px;
\theight: 25px;
}
.jugador {
\tposition: absolute;
\tfloat: left;
\twidth: 50px;
\theight: 50px;
\t
}
#tablero {
\tfloat: left;
}
#menu {
\tfloat: left;
}
</style>
<meta charset=\"utf-8\" />
<title>Trivial Nzabala</title>
<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
<script type=\"text/javascript\">
\t\$(document).ready(function(){

\t\tvar nJugadores = 3;
\t\tjugadores=[];
\t\tjugador={};
\t\tvar casillaCentral = [\"endstart\",400,400];

\t\tvar casillasCirculo = [[\"azul\",400,750,\"cruce\",\"\"],[\"amarillo\",472,738],[\"blanco\",514,732],[\"naranja\",552,711],[\"lila\",589,687],[\"blanco\",625,664],[\"verde\",660,631],[\"rosa\",703,574],[\"verde\",730,511],[\"blanco\",739,466],[\"amarillo\",745,421],[\"azul\",743,378],[\"lila\",730,292],[\"naranja\",702,223],[\"lila\",657,171],[\"blanco\",625,136],[\"verde\",589,110],[\"rosa\",552,90],[\"blanco\",513,70],[\"azul\",471,60],[\"amarillo\",400,48],[\"azul\",328,60],[\"blanco\",286,72],[\"lila\",246,88],[\"naranja\",207,111],[\"blanco\",174,136],[\"rosa\",141,170],[\"verde\",94,225],[\"rosa\",70,292],[\"blanco\",59,334],[\"azul\",52,379],[\"amarillo\",54,420],[\"blanco\",57,463],[\"naranja\",70,510],[\"lila\",96,576],[\"naranja\",139,630],[\"blanco\",174,661],[\"rosa\",207,687],[\"verde\",246,711],[\"blanco\",286,729],[\"amarillo\",328,741]];
\t\t
\t\tvar casillasVertical = [[\"azul\",400,105],[\"lila\",400,150],[\"rosa\",400,196],[\"naranja\",400,240],[\"verde\",400,285],[\"endstart\",400,400],[\"rosa\",400,515],[\"lila\",400,562],[\"verde\",400,599],[\"naranja\",400,646],[\"amarillo\",400,693]];

\t\tvar casillasHorizontalSup = [[\"rosa\",145,252],[\"azul\",184,276],[\"naranja\",222,297],[\"amarillo\",264,319],[\"lila\",300,342],[\"endstart\",400,400],[\"naranja\",500,459],[\"azul\",537,480],[\"lila\",576,502],[\"amarillo\",616,525],[\"verde\",657,546]];

\t\tvar casillasHorizontalInf = [[\"naranja\",142,547],[\"rosa\",183,523],[\"amarillo\",222,504],[\"verde\",259,478],[\"azul\",301,457],[\"endstart\",400,400],[\"amarillo\",500,343],[\"rosa\",537,319],[\"azul\",577,300],[\"verde\",616,277],[\"lila\",659,252]];

\t\t

\t\tvar objetivos =[];
\t\t\tobjetivos[\"azul\"]=[400,750];
\t\t\tobjetivos[\"rosa\"]=[703,574];
\t\t\tobjetivos[\"naranja\"]=[702,223];
\t\t\tobjetivos[\"amarillo\"]=[400,48];
\t\t\tobjetivos[\"verde\"]=[94,225];
\t\t\tobjetivos[\"lila\"]=[96,576];
\t\t\tobjetivos[\"fin\"]=[400,400];

\t\tfor(var i = 0; i < casillasVertical.length ;i++){
\t\t\tv = \"V\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionV\"+i, onclick:\"seleccionarCasilla(v,\"+i+\");\"}).css({top: casillasVertical[i][2], left: casillasVertical[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasCirculo.length ;i++){
\t\t\tc = \"C\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionC\"+i, onclick:\"seleccionarCasilla(c,\"+i+\");\"}).css({top: casillasCirculo[i][2], left: casillasCirculo[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasHorizontalSup.length ;i++){
\t\t\ths = \"HS\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionHS\"+i, onclick:\"seleccionarCasilla(hs,\"+i+\");\"}).css({top: casillasHorizontalSup[i][2], left: casillasHorizontalSup[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasHorizontalInf.length ;i++){
\t\t\thi = \"HI\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionHI\"+i, onclick:\"seleccionarCasilla(hi,\"+i+\");\"}).css({top: casillasHorizontalInf[i][2], left: casillasHorizontalInf[i][1]}));
\t\t}
\t\tempezarPartida(nJugadores);
\t});

\tfunction tirarDados(){
\t\tvar dado = Math.floor((Math.random() * 6) + 1); 
\t\t\$('#dadosText').text(dado);
\t\tvar valor = parseInt(\$('#rondaText').text());
\t\t\$('#rondaText').text(valor+1);
\t\t\$('#turno').text(jugadores[valor%3].id);
\t}
\tfunction seleccionarCasilla(ubicacion,id)
\t{
\t\tquien = \$('#turno').text();\t\t
\t\tdesde = \$(quien).position();
\t\thasta = \$(\"#posicion\"+ubicacion+id);

\t\tif (\$('#rondaText').text()!=0){
\t\t\tmoverQuesito(hasta.position());\t
\t\t}
\t}
\tfunction moverQuesito(posicionFicha){
\t\tficha = \$('#turno').text();
\t\t\$('#'+ficha).css({top: posicionFicha.top, left : posicionFicha.left});
\t}
\tfunction empezarPartida(nJugadores){
\t\tfor(var j = 0 ; j < nJugadores; j++){
\t\t\t\$('#tablero').append(\$(\"<img></img>\").addClass(\"jugador\").attr({id:\"jugador\"+j, src:\"/images/wow\"+j+\".png\"}).css({top: 400, left: 400}));
\t\t\tjugadores.push({id:\"jugador\"+j, posicion: \$('#jugador'+j).position()});
\t\t}
\t\t\$('#turno').text(jugadores[nJugadores%nJugadores].id);
\t\t
\t}

</script>
</head>
<body>
<div id=container>
\t<div id=tablero>
\t\t<img src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/images/trivial-wow.jpg"), "html", null, true);
        echo "\">
\t</div>
\t<div id=menu>
\t\t
\t\t<div id=ronda>
\t\t\t<p>Ronda Actual:</p>
\t\t\t<p id=rondaText>0</p>
\t\t\t<p>Turno Jugador:</p>
\t\t\t<p id=turno></p>
\t\t</div>
\t\t<div id=dados>
\t\t\t<button type=button onclick=\"tirarDados();\">Tirar Dados</button>
\t\t\t<p>Tirada:</p>
\t\t\t<p id=dadosText>NA</p>
\t\t</div>
\t\t<div id=score>
\t\t\t<p>Tu Puntuación:</p>
\t\t\t<p id=scoreText></p>
\t\t</div>
\t\t<div id=pregunta>
\t\t\t<p id=preguntaText></p>
\t\t</div>
\t\t<div id=tiempo>
\t\t\t<p></p>
\t\t</div>
\t</div>
</div>
</body>
</html>";
        
        $__internal_fac708cfa1e71ef0e4e092cabbf01d9737b268b63c1d86854856811e469c79a5->leave($__internal_fac708cfa1e71ef0e4e092cabbf01d9737b268b63c1d86854856811e469c79a5_prof);

        
        $__internal_52e9729b5f1f4204d9f63a56d5164f82037088d66dd1e09804ba56126d3646e1->leave($__internal_52e9729b5f1f4204d9f63a56d5164f82037088d66dd1e09804ba56126d3646e1_prof);

    }

    public function getTemplateName()
    {
        return "tablero.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 110,  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
<style>
.casilla {
\tposition: absolute;
\tfloat: left;
\twidth: 25px;
\theight: 25px;
}
.jugador {
\tposition: absolute;
\tfloat: left;
\twidth: 50px;
\theight: 50px;
\t
}
#tablero {
\tfloat: left;
}
#menu {
\tfloat: left;
}
</style>
<meta charset=\"utf-8\" />
<title>Trivial Nzabala</title>
<script src=\"{{ asset('bundles/fosjsrouting/js/router.js') }}\"></script>
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
<script type=\"text/javascript\">
\t\$(document).ready(function(){

\t\tvar nJugadores = 3;
\t\tjugadores=[];
\t\tjugador={};
\t\tvar casillaCentral = [\"endstart\",400,400];

\t\tvar casillasCirculo = [[\"azul\",400,750,\"cruce\",\"\"],[\"amarillo\",472,738],[\"blanco\",514,732],[\"naranja\",552,711],[\"lila\",589,687],[\"blanco\",625,664],[\"verde\",660,631],[\"rosa\",703,574],[\"verde\",730,511],[\"blanco\",739,466],[\"amarillo\",745,421],[\"azul\",743,378],[\"lila\",730,292],[\"naranja\",702,223],[\"lila\",657,171],[\"blanco\",625,136],[\"verde\",589,110],[\"rosa\",552,90],[\"blanco\",513,70],[\"azul\",471,60],[\"amarillo\",400,48],[\"azul\",328,60],[\"blanco\",286,72],[\"lila\",246,88],[\"naranja\",207,111],[\"blanco\",174,136],[\"rosa\",141,170],[\"verde\",94,225],[\"rosa\",70,292],[\"blanco\",59,334],[\"azul\",52,379],[\"amarillo\",54,420],[\"blanco\",57,463],[\"naranja\",70,510],[\"lila\",96,576],[\"naranja\",139,630],[\"blanco\",174,661],[\"rosa\",207,687],[\"verde\",246,711],[\"blanco\",286,729],[\"amarillo\",328,741]];
\t\t
\t\tvar casillasVertical = [[\"azul\",400,105],[\"lila\",400,150],[\"rosa\",400,196],[\"naranja\",400,240],[\"verde\",400,285],[\"endstart\",400,400],[\"rosa\",400,515],[\"lila\",400,562],[\"verde\",400,599],[\"naranja\",400,646],[\"amarillo\",400,693]];

\t\tvar casillasHorizontalSup = [[\"rosa\",145,252],[\"azul\",184,276],[\"naranja\",222,297],[\"amarillo\",264,319],[\"lila\",300,342],[\"endstart\",400,400],[\"naranja\",500,459],[\"azul\",537,480],[\"lila\",576,502],[\"amarillo\",616,525],[\"verde\",657,546]];

\t\tvar casillasHorizontalInf = [[\"naranja\",142,547],[\"rosa\",183,523],[\"amarillo\",222,504],[\"verde\",259,478],[\"azul\",301,457],[\"endstart\",400,400],[\"amarillo\",500,343],[\"rosa\",537,319],[\"azul\",577,300],[\"verde\",616,277],[\"lila\",659,252]];

\t\t

\t\tvar objetivos =[];
\t\t\tobjetivos[\"azul\"]=[400,750];
\t\t\tobjetivos[\"rosa\"]=[703,574];
\t\t\tobjetivos[\"naranja\"]=[702,223];
\t\t\tobjetivos[\"amarillo\"]=[400,48];
\t\t\tobjetivos[\"verde\"]=[94,225];
\t\t\tobjetivos[\"lila\"]=[96,576];
\t\t\tobjetivos[\"fin\"]=[400,400];

\t\tfor(var i = 0; i < casillasVertical.length ;i++){
\t\t\tv = \"V\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionV\"+i, onclick:\"seleccionarCasilla(v,\"+i+\");\"}).css({top: casillasVertical[i][2], left: casillasVertical[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasCirculo.length ;i++){
\t\t\tc = \"C\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionC\"+i, onclick:\"seleccionarCasilla(c,\"+i+\");\"}).css({top: casillasCirculo[i][2], left: casillasCirculo[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasHorizontalSup.length ;i++){
\t\t\ths = \"HS\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionHS\"+i, onclick:\"seleccionarCasilla(hs,\"+i+\");\"}).css({top: casillasHorizontalSup[i][2], left: casillasHorizontalSup[i][1]}));
\t\t}
\t\tfor(var i = 0; i < casillasHorizontalInf.length ;i++){
\t\t\thi = \"HI\";
\t\t\t\$('#tablero').append(\$(\"<div></div>\").addClass(\"casilla\").attr({id:\"posicionHI\"+i, onclick:\"seleccionarCasilla(hi,\"+i+\");\"}).css({top: casillasHorizontalInf[i][2], left: casillasHorizontalInf[i][1]}));
\t\t}
\t\tempezarPartida(nJugadores);
\t});

\tfunction tirarDados(){
\t\tvar dado = Math.floor((Math.random() * 6) + 1); 
\t\t\$('#dadosText').text(dado);
\t\tvar valor = parseInt(\$('#rondaText').text());
\t\t\$('#rondaText').text(valor+1);
\t\t\$('#turno').text(jugadores[valor%3].id);
\t}
\tfunction seleccionarCasilla(ubicacion,id)
\t{
\t\tquien = \$('#turno').text();\t\t
\t\tdesde = \$(quien).position();
\t\thasta = \$(\"#posicion\"+ubicacion+id);

\t\tif (\$('#rondaText').text()!=0){
\t\t\tmoverQuesito(hasta.position());\t
\t\t}
\t}
\tfunction moverQuesito(posicionFicha){
\t\tficha = \$('#turno').text();
\t\t\$('#'+ficha).css({top: posicionFicha.top, left : posicionFicha.left});
\t}
\tfunction empezarPartida(nJugadores){
\t\tfor(var j = 0 ; j < nJugadores; j++){
\t\t\t\$('#tablero').append(\$(\"<img></img>\").addClass(\"jugador\").attr({id:\"jugador\"+j, src:\"/images/wow\"+j+\".png\"}).css({top: 400, left: 400}));
\t\t\tjugadores.push({id:\"jugador\"+j, posicion: \$('#jugador'+j).position()});
\t\t}
\t\t\$('#turno').text(jugadores[nJugadores%nJugadores].id);
\t\t
\t}

</script>
</head>
<body>
<div id=container>
\t<div id=tablero>
\t\t<img src=\"{{ asset('/images/trivial-wow.jpg') }}\">
\t</div>
\t<div id=menu>
\t\t
\t\t<div id=ronda>
\t\t\t<p>Ronda Actual:</p>
\t\t\t<p id=rondaText>0</p>
\t\t\t<p>Turno Jugador:</p>
\t\t\t<p id=turno></p>
\t\t</div>
\t\t<div id=dados>
\t\t\t<button type=button onclick=\"tirarDados();\">Tirar Dados</button>
\t\t\t<p>Tirada:</p>
\t\t\t<p id=dadosText>NA</p>
\t\t</div>
\t\t<div id=score>
\t\t\t<p>Tu Puntuación:</p>
\t\t\t<p id=scoreText></p>
\t\t</div>
\t\t<div id=pregunta>
\t\t\t<p id=preguntaText></p>
\t\t</div>
\t\t<div id=tiempo>
\t\t\t<p></p>
\t\t</div>
\t</div>
</div>
</body>
</html>", "tablero.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/tablero.html.twig");
    }
}
