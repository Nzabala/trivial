<?php

/* default/index.html.twig */
class __TwigTemplate_383b68435a2dbe425c8bc1b2510f9183d08b712ee4083989d746cb1e708ba59b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
            'upperbar2' => array($this, 'block_upperbar2'),
            'upperbar3' => array($this, 'block_upperbar3'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_57cfe01a723366c4bd72cc2e4a8ca5332bd1da57a91a227a6027de1502d16bde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57cfe01a723366c4bd72cc2e4a8ca5332bd1da57a91a227a6027de1502d16bde->enter($__internal_57cfe01a723366c4bd72cc2e4a8ca5332bd1da57a91a227a6027de1502d16bde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_24d8c229bc497959bbef65ab1a0cbfc845da6c7797ed680ace0db03edc0ebc08 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24d8c229bc497959bbef65ab1a0cbfc845da6c7797ed680ace0db03edc0ebc08->enter($__internal_24d8c229bc497959bbef65ab1a0cbfc845da6c7797ed680ace0db03edc0ebc08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_57cfe01a723366c4bd72cc2e4a8ca5332bd1da57a91a227a6027de1502d16bde->leave($__internal_57cfe01a723366c4bd72cc2e4a8ca5332bd1da57a91a227a6027de1502d16bde_prof);

        
        $__internal_24d8c229bc497959bbef65ab1a0cbfc845da6c7797ed680ace0db03edc0ebc08->leave($__internal_24d8c229bc497959bbef65ab1a0cbfc845da6c7797ed680ace0db03edc0ebc08_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_fca0fe10176e05a535f0289b42a8c4bd29154e7c23cfe86c3f06874898b122cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fca0fe10176e05a535f0289b42a8c4bd29154e7c23cfe86c3f06874898b122cc->enter($__internal_fca0fe10176e05a535f0289b42a8c4bd29154e7c23cfe86c3f06874898b122cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fc72efad5386430a35458aabb1dd1602811860615ef7c97425032718897c78d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc72efad5386430a35458aabb1dd1602811860615ef7c97425032718897c78d0->enter($__internal_fc72efad5386430a35458aabb1dd1602811860615ef7c97425032718897c78d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "   <h3>Trivial Menu</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 14
        echo "        </div>
    <h4>Colabora en nuestra comunidad de preguntas para conseguir cada mes nuevas y más divertidas preguntas para nuestro Trivial Online</h4>
    <h3>Menu preguntas</h3>
        <div id=\"upperbar2\">
            ";
        // line 18
        $this->displayBlock('upperbar2', $context, $blocks);
        // line 24
        echo "        </div>
    <h3>Menu partidas</h3>
        <div id=\"upperbar3\">
            ";
        // line 27
        $this->displayBlock('upperbar3', $context, $blocks);
        // line 32
        echo "        </div>
";
        
        $__internal_fc72efad5386430a35458aabb1dd1602811860615ef7c97425032718897c78d0->leave($__internal_fc72efad5386430a35458aabb1dd1602811860615ef7c97425032718897c78d0_prof);

        
        $__internal_fca0fe10176e05a535f0289b42a8c4bd29154e7c23cfe86c3f06874898b122cc->leave($__internal_fca0fe10176e05a535f0289b42a8c4bd29154e7c23cfe86c3f06874898b122cc_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_79beae1ed118d8ce5c76988bc48e2e96d09022c24f8cbdb362ce65cd128d9d90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79beae1ed118d8ce5c76988bc48e2e96d09022c24f8cbdb362ce65cd128d9d90->enter($__internal_79beae1ed118d8ce5c76988bc48e2e96d09022c24f8cbdb362ce65cd128d9d90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        $__internal_2291ad26b2006ad9faca9fa0c276f2b9701d94ce5dddc5e3ef32a6076a270e3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2291ad26b2006ad9faca9fa0c276f2b9701d94ce5dddc5e3ef32a6076a270e3d->enter($__internal_2291ad26b2006ad9faca9fa0c276f2b9701d94ce5dddc5e3ef32a6076a270e3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/showGames\">Jugar Ahora</a>
                <a href=\"/showAllPlayers\">Mostrar Ranking</a>
                <a href=\"/showUser\">Buscar Jugador</a>
                <a href=\"/editUser\">Editar Jugador</a>
                <a href=\"/deleteUser\">Eliminar Jugador</a>
                <br>
            ";
        
        $__internal_2291ad26b2006ad9faca9fa0c276f2b9701d94ce5dddc5e3ef32a6076a270e3d->leave($__internal_2291ad26b2006ad9faca9fa0c276f2b9701d94ce5dddc5e3ef32a6076a270e3d_prof);

        
        $__internal_79beae1ed118d8ce5c76988bc48e2e96d09022c24f8cbdb362ce65cd128d9d90->leave($__internal_79beae1ed118d8ce5c76988bc48e2e96d09022c24f8cbdb362ce65cd128d9d90_prof);

    }

    // line 18
    public function block_upperbar2($context, array $blocks = array())
    {
        $__internal_1154205199d56767aecec623b13bdedfee410ae69b7d2a2900bd9ff727c13eda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1154205199d56767aecec623b13bdedfee410ae69b7d2a2900bd9ff727c13eda->enter($__internal_1154205199d56767aecec623b13bdedfee410ae69b7d2a2900bd9ff727c13eda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar2"));

        $__internal_c60b504f48a48a9c1b5a9c58afcd116a01e9b443be6e8281a24f4cc2002e9a05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c60b504f48a48a9c1b5a9c58afcd116a01e9b443be6e8281a24f4cc2002e9a05->enter($__internal_c60b504f48a48a9c1b5a9c58afcd116a01e9b443be6e8281a24f4cc2002e9a05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar2"));

        // line 19
        echo "                <a href=\"/newQuiz\">Añadir Preguntas</a>
                <a href=\"/showQuiz\">Buscar Pregunta</a>
                <a href=\"/editQuiz\">Editar Preguntas</a>
                <br>
            ";
        
        $__internal_c60b504f48a48a9c1b5a9c58afcd116a01e9b443be6e8281a24f4cc2002e9a05->leave($__internal_c60b504f48a48a9c1b5a9c58afcd116a01e9b443be6e8281a24f4cc2002e9a05_prof);

        
        $__internal_1154205199d56767aecec623b13bdedfee410ae69b7d2a2900bd9ff727c13eda->leave($__internal_1154205199d56767aecec623b13bdedfee410ae69b7d2a2900bd9ff727c13eda_prof);

    }

    // line 27
    public function block_upperbar3($context, array $blocks = array())
    {
        $__internal_9a9afefe02a20bc7901490019bcc36150fbefd99dbc85676a8260021b150453b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a9afefe02a20bc7901490019bcc36150fbefd99dbc85676a8260021b150453b->enter($__internal_9a9afefe02a20bc7901490019bcc36150fbefd99dbc85676a8260021b150453b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar3"));

        $__internal_fab2edcd81534522bb701ab00c647ea43a9d7c6dbb2eeadef1bc343a3ee179f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fab2edcd81534522bb701ab00c647ea43a9d7c6dbb2eeadef1bc343a3ee179f4->enter($__internal_fab2edcd81534522bb701ab00c647ea43a9d7c6dbb2eeadef1bc343a3ee179f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar3"));

        // line 28
        echo "                <a href=\"/searchGame\">Buscar Partida</a>
                <a href=\"/deleteGame\">Eliminar Partida</a>
                <br>
            ";
        
        $__internal_fab2edcd81534522bb701ab00c647ea43a9d7c6dbb2eeadef1bc343a3ee179f4->leave($__internal_fab2edcd81534522bb701ab00c647ea43a9d7c6dbb2eeadef1bc343a3ee179f4_prof);

        
        $__internal_9a9afefe02a20bc7901490019bcc36150fbefd99dbc85676a8260021b150453b->leave($__internal_9a9afefe02a20bc7901490019bcc36150fbefd99dbc85676a8260021b150453b_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 28,  132 => 27,  118 => 19,  109 => 18,  93 => 7,  84 => 6,  73 => 32,  71 => 27,  66 => 24,  64 => 18,  58 => 14,  56 => 6,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
   <h3>Trivial Menu</h3>
        <div id=\"upperbar1\">
            {% block upperbar1 %}
                <a href=\"/showGames\">Jugar Ahora</a>
                <a href=\"/showAllPlayers\">Mostrar Ranking</a>
                <a href=\"/showUser\">Buscar Jugador</a>
                <a href=\"/editUser\">Editar Jugador</a>
                <a href=\"/deleteUser\">Eliminar Jugador</a>
                <br>
            {% endblock %}
        </div>
    <h4>Colabora en nuestra comunidad de preguntas para conseguir cada mes nuevas y más divertidas preguntas para nuestro Trivial Online</h4>
    <h3>Menu preguntas</h3>
        <div id=\"upperbar2\">
            {% block upperbar2 %}
                <a href=\"/newQuiz\">Añadir Preguntas</a>
                <a href=\"/showQuiz\">Buscar Pregunta</a>
                <a href=\"/editQuiz\">Editar Preguntas</a>
                <br>
            {% endblock %}
        </div>
    <h3>Menu partidas</h3>
        <div id=\"upperbar3\">
            {% block upperbar3 %}
                <a href=\"/searchGame\">Buscar Partida</a>
                <a href=\"/deleteGame\">Eliminar Partida</a>
                <br>
            {% endblock %}
        </div>
{% endblock %}
", "default/index.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/default/index.html.twig");
    }
}
