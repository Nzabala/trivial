<?php

/* base.html.twig */
class __TwigTemplate_d81bae7d5325a7f23ba92e849144467ab8ed2421c201cbf1d70c17582ad76de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'message' => array($this, 'block_message'),
            'message2' => array($this, 'block_message2'),
            'form' => array($this, 'block_form'),
            'buttonLogin' => array($this, 'block_buttonLogin'),
            'button' => array($this, 'block_button'),
            'backButton' => array($this, 'block_backButton'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5542d6e8308be63cf12687762e5b6519c65a0a6d37690d0f53d416a988f3fbac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5542d6e8308be63cf12687762e5b6519c65a0a6d37690d0f53d416a988f3fbac->enter($__internal_5542d6e8308be63cf12687762e5b6519c65a0a6d37690d0f53d416a988f3fbac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_5ceef6c3069d6e92080a26cf5213b03ff35b76ba7c9ebc4611511a7c1be4b575 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ceef6c3069d6e92080a26cf5213b03ff35b76ba7c9ebc4611511a7c1be4b575->enter($__internal_5ceef6c3069d6e92080a26cf5213b03ff35b76ba7c9ebc4611511a7c1be4b575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    \t";
        // line 13
        $this->displayBlock('header', $context, $blocks);
        // line 17
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "        ";
        $this->displayBlock('message', $context, $blocks);
        // line 19
        echo "        ";
        $this->displayBlock('message2', $context, $blocks);
        // line 20
        echo "        ";
        $this->displayBlock('form', $context, $blocks);
        // line 21
        echo "        ";
        $this->displayBlock('buttonLogin', $context, $blocks);
        // line 22
        echo "        ";
        $this->displayBlock('button', $context, $blocks);
        // line 23
        echo "        ";
        $this->displayBlock('backButton', $context, $blocks);
        // line 24
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 25
        echo "    </body>
</html>
";
        
        $__internal_5542d6e8308be63cf12687762e5b6519c65a0a6d37690d0f53d416a988f3fbac->leave($__internal_5542d6e8308be63cf12687762e5b6519c65a0a6d37690d0f53d416a988f3fbac_prof);

        
        $__internal_5ceef6c3069d6e92080a26cf5213b03ff35b76ba7c9ebc4611511a7c1be4b575->leave($__internal_5ceef6c3069d6e92080a26cf5213b03ff35b76ba7c9ebc4611511a7c1be4b575_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9a958f2ed05c28c599a67486c8aab8946f4bc785076acdc7a4c1753bde375a5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a958f2ed05c28c599a67486c8aab8946f4bc785076acdc7a4c1753bde375a5c->enter($__internal_9a958f2ed05c28c599a67486c8aab8946f4bc785076acdc7a4c1753bde375a5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_fae3d7733ff17a8255baf540abd4a62d172f2c7be19318158090f37226455843 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fae3d7733ff17a8255baf540abd4a62d172f2c7be19318158090f37226455843->enter($__internal_fae3d7733ff17a8255baf540abd4a62d172f2c7be19318158090f37226455843_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_fae3d7733ff17a8255baf540abd4a62d172f2c7be19318158090f37226455843->leave($__internal_fae3d7733ff17a8255baf540abd4a62d172f2c7be19318158090f37226455843_prof);

        
        $__internal_9a958f2ed05c28c599a67486c8aab8946f4bc785076acdc7a4c1753bde375a5c->leave($__internal_9a958f2ed05c28c599a67486c8aab8946f4bc785076acdc7a4c1753bde375a5c_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0be267cf53eae239d2c5b813f95c2236fa3753afdc64efb2a080a92e657d2045 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0be267cf53eae239d2c5b813f95c2236fa3753afdc64efb2a080a92e657d2045->enter($__internal_0be267cf53eae239d2c5b813f95c2236fa3753afdc64efb2a080a92e657d2045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_0f2f705238e618953fef781645cccb6b9eb472859313a2f9e1547caa9e700d53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f2f705238e618953fef781645cccb6b9eb472859313a2f9e1547caa9e700d53->enter($__internal_0f2f705238e618953fef781645cccb6b9eb472859313a2f9e1547caa9e700d53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\"/>
        ";
        
        $__internal_0f2f705238e618953fef781645cccb6b9eb472859313a2f9e1547caa9e700d53->leave($__internal_0f2f705238e618953fef781645cccb6b9eb472859313a2f9e1547caa9e700d53_prof);

        
        $__internal_0be267cf53eae239d2c5b813f95c2236fa3753afdc64efb2a080a92e657d2045->leave($__internal_0be267cf53eae239d2c5b813f95c2236fa3753afdc64efb2a080a92e657d2045_prof);

    }

    // line 13
    public function block_header($context, array $blocks = array())
    {
        $__internal_66cc24417e4e4cbae3772f7f96ce8c31cd1e9a2ee4148893cfd2304a697bc0c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66cc24417e4e4cbae3772f7f96ce8c31cd1e9a2ee4148893cfd2304a697bc0c1->enter($__internal_66cc24417e4e4cbae3772f7f96ce8c31cd1e9a2ee4148893cfd2304a697bc0c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_657218ff0f5dbbf6eaecf0de9b0af90f9ef17ad167604ebcd2ca568c31e7d5df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_657218ff0f5dbbf6eaecf0de9b0af90f9ef17ad167604ebcd2ca568c31e7d5df->enter($__internal_657218ff0f5dbbf6eaecf0de9b0af90f9ef17ad167604ebcd2ca568c31e7d5df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 14
        echo "\t\t\t<h1> Desafía a tus compañeros en este TRIVIAL Online </h1>
            <h2><a href=\"/\">Acceder al Hall</a></h2>    \t  
    \t";
        
        $__internal_657218ff0f5dbbf6eaecf0de9b0af90f9ef17ad167604ebcd2ca568c31e7d5df->leave($__internal_657218ff0f5dbbf6eaecf0de9b0af90f9ef17ad167604ebcd2ca568c31e7d5df_prof);

        
        $__internal_66cc24417e4e4cbae3772f7f96ce8c31cd1e9a2ee4148893cfd2304a697bc0c1->leave($__internal_66cc24417e4e4cbae3772f7f96ce8c31cd1e9a2ee4148893cfd2304a697bc0c1_prof);

    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        $__internal_7df7cae612bfd0412581c429f7dd42624b4faf7f11274883910fb16831bb89cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7df7cae612bfd0412581c429f7dd42624b4faf7f11274883910fb16831bb89cc->enter($__internal_7df7cae612bfd0412581c429f7dd42624b4faf7f11274883910fb16831bb89cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ec58cb6232d4a45c39ccdf172083556c6e4f2c907912990d658257ef36adae07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec58cb6232d4a45c39ccdf172083556c6e4f2c907912990d658257ef36adae07->enter($__internal_ec58cb6232d4a45c39ccdf172083556c6e4f2c907912990d658257ef36adae07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_ec58cb6232d4a45c39ccdf172083556c6e4f2c907912990d658257ef36adae07->leave($__internal_ec58cb6232d4a45c39ccdf172083556c6e4f2c907912990d658257ef36adae07_prof);

        
        $__internal_7df7cae612bfd0412581c429f7dd42624b4faf7f11274883910fb16831bb89cc->leave($__internal_7df7cae612bfd0412581c429f7dd42624b4faf7f11274883910fb16831bb89cc_prof);

    }

    // line 18
    public function block_message($context, array $blocks = array())
    {
        $__internal_53aecdeae72f5985c5fa012339b2ed6ae6bc710ff29550d7f50ec3da61ca6355 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53aecdeae72f5985c5fa012339b2ed6ae6bc710ff29550d7f50ec3da61ca6355->enter($__internal_53aecdeae72f5985c5fa012339b2ed6ae6bc710ff29550d7f50ec3da61ca6355_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message"));

        $__internal_9d62ac3342f9da228664405da8f94478896fce26bdb0891acfd34c675be9963c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d62ac3342f9da228664405da8f94478896fce26bdb0891acfd34c675be9963c->enter($__internal_9d62ac3342f9da228664405da8f94478896fce26bdb0891acfd34c675be9963c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message"));

        
        $__internal_9d62ac3342f9da228664405da8f94478896fce26bdb0891acfd34c675be9963c->leave($__internal_9d62ac3342f9da228664405da8f94478896fce26bdb0891acfd34c675be9963c_prof);

        
        $__internal_53aecdeae72f5985c5fa012339b2ed6ae6bc710ff29550d7f50ec3da61ca6355->leave($__internal_53aecdeae72f5985c5fa012339b2ed6ae6bc710ff29550d7f50ec3da61ca6355_prof);

    }

    // line 19
    public function block_message2($context, array $blocks = array())
    {
        $__internal_9f7ef19dff4833018c6d5329013c517eea52e73c89b38b730035bdb549f4fdfa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f7ef19dff4833018c6d5329013c517eea52e73c89b38b730035bdb549f4fdfa->enter($__internal_9f7ef19dff4833018c6d5329013c517eea52e73c89b38b730035bdb549f4fdfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message2"));

        $__internal_506ea1a5fc2854f933f47d1e0f4cf6fcdf8792909cf078ff18c3b1dc1dafdfc2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_506ea1a5fc2854f933f47d1e0f4cf6fcdf8792909cf078ff18c3b1dc1dafdfc2->enter($__internal_506ea1a5fc2854f933f47d1e0f4cf6fcdf8792909cf078ff18c3b1dc1dafdfc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "message2"));

        
        $__internal_506ea1a5fc2854f933f47d1e0f4cf6fcdf8792909cf078ff18c3b1dc1dafdfc2->leave($__internal_506ea1a5fc2854f933f47d1e0f4cf6fcdf8792909cf078ff18c3b1dc1dafdfc2_prof);

        
        $__internal_9f7ef19dff4833018c6d5329013c517eea52e73c89b38b730035bdb549f4fdfa->leave($__internal_9f7ef19dff4833018c6d5329013c517eea52e73c89b38b730035bdb549f4fdfa_prof);

    }

    // line 20
    public function block_form($context, array $blocks = array())
    {
        $__internal_eaeb9fb45c251c26ef45388fe88c8812d7502bcbd58f2576c8e808082f183c62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaeb9fb45c251c26ef45388fe88c8812d7502bcbd58f2576c8e808082f183c62->enter($__internal_eaeb9fb45c251c26ef45388fe88c8812d7502bcbd58f2576c8e808082f183c62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_b56a88e3e888a0e541da94eaedaf9c4f7d48b5dcaa99fc871eb8f60fa95c3891 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b56a88e3e888a0e541da94eaedaf9c4f7d48b5dcaa99fc871eb8f60fa95c3891->enter($__internal_b56a88e3e888a0e541da94eaedaf9c4f7d48b5dcaa99fc871eb8f60fa95c3891_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        
        $__internal_b56a88e3e888a0e541da94eaedaf9c4f7d48b5dcaa99fc871eb8f60fa95c3891->leave($__internal_b56a88e3e888a0e541da94eaedaf9c4f7d48b5dcaa99fc871eb8f60fa95c3891_prof);

        
        $__internal_eaeb9fb45c251c26ef45388fe88c8812d7502bcbd58f2576c8e808082f183c62->leave($__internal_eaeb9fb45c251c26ef45388fe88c8812d7502bcbd58f2576c8e808082f183c62_prof);

    }

    // line 21
    public function block_buttonLogin($context, array $blocks = array())
    {
        $__internal_682a2bb97f2b667fb08b13da2bafe0c8110162d6c0e607e0f78365bfc479dc76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_682a2bb97f2b667fb08b13da2bafe0c8110162d6c0e607e0f78365bfc479dc76->enter($__internal_682a2bb97f2b667fb08b13da2bafe0c8110162d6c0e607e0f78365bfc479dc76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        $__internal_877ee452b1600b469fd1cdba861ef8244d79d5e914accb25d78587cc8ebd73e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_877ee452b1600b469fd1cdba861ef8244d79d5e914accb25d78587cc8ebd73e5->enter($__internal_877ee452b1600b469fd1cdba861ef8244d79d5e914accb25d78587cc8ebd73e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        
        $__internal_877ee452b1600b469fd1cdba861ef8244d79d5e914accb25d78587cc8ebd73e5->leave($__internal_877ee452b1600b469fd1cdba861ef8244d79d5e914accb25d78587cc8ebd73e5_prof);

        
        $__internal_682a2bb97f2b667fb08b13da2bafe0c8110162d6c0e607e0f78365bfc479dc76->leave($__internal_682a2bb97f2b667fb08b13da2bafe0c8110162d6c0e607e0f78365bfc479dc76_prof);

    }

    // line 22
    public function block_button($context, array $blocks = array())
    {
        $__internal_2a5c73d224192041ab542f2a8f6de1da268b160baa7eb200e7755a9e926628b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a5c73d224192041ab542f2a8f6de1da268b160baa7eb200e7755a9e926628b2->enter($__internal_2a5c73d224192041ab542f2a8f6de1da268b160baa7eb200e7755a9e926628b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button"));

        $__internal_864eeba5cc70bb85a21446de689650d08fcd26a1600293b4f5123b65ee3253c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_864eeba5cc70bb85a21446de689650d08fcd26a1600293b4f5123b65ee3253c3->enter($__internal_864eeba5cc70bb85a21446de689650d08fcd26a1600293b4f5123b65ee3253c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button"));

        
        $__internal_864eeba5cc70bb85a21446de689650d08fcd26a1600293b4f5123b65ee3253c3->leave($__internal_864eeba5cc70bb85a21446de689650d08fcd26a1600293b4f5123b65ee3253c3_prof);

        
        $__internal_2a5c73d224192041ab542f2a8f6de1da268b160baa7eb200e7755a9e926628b2->leave($__internal_2a5c73d224192041ab542f2a8f6de1da268b160baa7eb200e7755a9e926628b2_prof);

    }

    // line 23
    public function block_backButton($context, array $blocks = array())
    {
        $__internal_01db2b9af98738de6c95bfba624a99ebfc2a508c7987a63026a68730d8906fdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01db2b9af98738de6c95bfba624a99ebfc2a508c7987a63026a68730d8906fdb->enter($__internal_01db2b9af98738de6c95bfba624a99ebfc2a508c7987a63026a68730d8906fdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        $__internal_246c67d48cbd9fbf5e84a566f8a8a476e06aaa14d34beeae5b286d05040bbaf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_246c67d48cbd9fbf5e84a566f8a8a476e06aaa14d34beeae5b286d05040bbaf0->enter($__internal_246c67d48cbd9fbf5e84a566f8a8a476e06aaa14d34beeae5b286d05040bbaf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        
        $__internal_246c67d48cbd9fbf5e84a566f8a8a476e06aaa14d34beeae5b286d05040bbaf0->leave($__internal_246c67d48cbd9fbf5e84a566f8a8a476e06aaa14d34beeae5b286d05040bbaf0_prof);

        
        $__internal_01db2b9af98738de6c95bfba624a99ebfc2a508c7987a63026a68730d8906fdb->leave($__internal_01db2b9af98738de6c95bfba624a99ebfc2a508c7987a63026a68730d8906fdb_prof);

    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_50e562172845a851e69dd3be53b86fab38ca47f525690eec2370e5041ad6349c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50e562172845a851e69dd3be53b86fab38ca47f525690eec2370e5041ad6349c->enter($__internal_50e562172845a851e69dd3be53b86fab38ca47f525690eec2370e5041ad6349c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5c196c87f0eccd0ef7f31d341a7cb56c583ac81e1be02d9e5a383a5e9eeae611 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c196c87f0eccd0ef7f31d341a7cb56c583ac81e1be02d9e5a383a5e9eeae611->enter($__internal_5c196c87f0eccd0ef7f31d341a7cb56c583ac81e1be02d9e5a383a5e9eeae611_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5c196c87f0eccd0ef7f31d341a7cb56c583ac81e1be02d9e5a383a5e9eeae611->leave($__internal_5c196c87f0eccd0ef7f31d341a7cb56c583ac81e1be02d9e5a383a5e9eeae611_prof);

        
        $__internal_50e562172845a851e69dd3be53b86fab38ca47f525690eec2370e5041ad6349c->leave($__internal_50e562172845a851e69dd3be53b86fab38ca47f525690eec2370e5041ad6349c_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  275 => 24,  258 => 23,  241 => 22,  224 => 21,  207 => 20,  190 => 19,  173 => 18,  156 => 17,  144 => 14,  135 => 13,  122 => 7,  113 => 6,  95 => 5,  83 => 25,  80 => 24,  77 => 23,  74 => 22,  71 => 21,  68 => 20,  65 => 19,  62 => 18,  59 => 17,  57 => 13,  51 => 10,  48 => 9,  46 => 6,  42 => 5,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" type=\"text/css\" rel=\"stylesheet\"/>
        {% endblock %}

        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    \t{% block header %}
\t\t\t<h1> Desafía a tus compañeros en este TRIVIAL Online </h1>
            <h2><a href=\"/\">Acceder al Hall</a></h2>    \t  
    \t{% endblock %}
        {% block body %}{% endblock %}
        {% block message %}{% endblock %}
        {% block message2 %}{% endblock %}
        {% block form %}{% endblock %}
        {% block buttonLogin %}{% endblock %}
        {% block button %}{% endblock %}
        {% block backButton %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/base.html.twig");
    }
}
