<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_4c6dcf6dedb99871b79f2b64c6c884e9cfcee01d0a1366649f7cc7676244a7f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'header' => array($this, 'block_header'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34456c1a55c796cabd8e1b1ebece0100cfa42b489c9a3ec6ccdba30aa20a2936 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34456c1a55c796cabd8e1b1ebece0100cfa42b489c9a3ec6ccdba30aa20a2936->enter($__internal_34456c1a55c796cabd8e1b1ebece0100cfa42b489c9a3ec6ccdba30aa20a2936_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_845485d86a0ac748586966c3851b652d52b60aba20bf8e52d1019a7269957463 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_845485d86a0ac748586966c3851b652d52b60aba20bf8e52d1019a7269957463->enter($__internal_845485d86a0ac748586966c3851b652d52b60aba20bf8e52d1019a7269957463_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        ";
        // line 5
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 8
        echo "    </head>

    <body>
        ";
        // line 11
        $this->displayBlock('header', $context, $blocks);
        // line 14
        echo "        <div>
            ";
        // line 15
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 16
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
                <a href=\"";
            // line 17
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                    ";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
            ";
        } else {
            // line 21
            echo "                <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                <a href=\"";
            // line 22
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
            ";
        }
        // line 24
        echo "        </div>


        ";
        // line 27
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 28
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 29
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 30
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        <div class=\"message\">
                            ";
                    // line 32
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                        </div>
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "        ";
        }
        // line 38
        echo "
        <div>
            ";
        // line 40
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 42
        echo "        </div>
        ";
        // line 43
        $this->displayBlock('javascripts', $context, $blocks);
        // line 44
        echo "    </body>
</html>
";
        
        $__internal_34456c1a55c796cabd8e1b1ebece0100cfa42b489c9a3ec6ccdba30aa20a2936->leave($__internal_34456c1a55c796cabd8e1b1ebece0100cfa42b489c9a3ec6ccdba30aa20a2936_prof);

        
        $__internal_845485d86a0ac748586966c3851b652d52b60aba20bf8e52d1019a7269957463->leave($__internal_845485d86a0ac748586966c3851b652d52b60aba20bf8e52d1019a7269957463_prof);

    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b07feefdec735e03ca875d38fa3c155e7eabce78b7f13a5f13273cc3aec450ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b07feefdec735e03ca875d38fa3c155e7eabce78b7f13a5f13273cc3aec450ab->enter($__internal_b07feefdec735e03ca875d38fa3c155e7eabce78b7f13a5f13273cc3aec450ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_611afb0330d68d12c13460d906dcf6a37ca93b70ab25db7a364e367213b85eb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_611afb0330d68d12c13460d906dcf6a37ca93b70ab25db7a364e367213b85eb1->enter($__internal_611afb0330d68d12c13460d906dcf6a37ca93b70ab25db7a364e367213b85eb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 6
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\"/>
        ";
        
        $__internal_611afb0330d68d12c13460d906dcf6a37ca93b70ab25db7a364e367213b85eb1->leave($__internal_611afb0330d68d12c13460d906dcf6a37ca93b70ab25db7a364e367213b85eb1_prof);

        
        $__internal_b07feefdec735e03ca875d38fa3c155e7eabce78b7f13a5f13273cc3aec450ab->leave($__internal_b07feefdec735e03ca875d38fa3c155e7eabce78b7f13a5f13273cc3aec450ab_prof);

    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        $__internal_07b8b1fad6f00e11578dbbf5314909015b13d954dbd10b469c10020a1b825497 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07b8b1fad6f00e11578dbbf5314909015b13d954dbd10b469c10020a1b825497->enter($__internal_07b8b1fad6f00e11578dbbf5314909015b13d954dbd10b469c10020a1b825497_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_47cc2e273e5b5fd293f7af4e71364c03fa20f096f555b01ed95a64de57088c96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47cc2e273e5b5fd293f7af4e71364c03fa20f096f555b01ed95a64de57088c96->enter($__internal_47cc2e273e5b5fd293f7af4e71364c03fa20f096f555b01ed95a64de57088c96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 12
        echo "            <h1> Desafía a tus compañeros en este TRIVIAL Online </h1>        
        ";
        
        $__internal_47cc2e273e5b5fd293f7af4e71364c03fa20f096f555b01ed95a64de57088c96->leave($__internal_47cc2e273e5b5fd293f7af4e71364c03fa20f096f555b01ed95a64de57088c96_prof);

        
        $__internal_07b8b1fad6f00e11578dbbf5314909015b13d954dbd10b469c10020a1b825497->leave($__internal_07b8b1fad6f00e11578dbbf5314909015b13d954dbd10b469c10020a1b825497_prof);

    }

    // line 40
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_56f15439193d229a6e717b8f3cf696fbcece0e2f1eb17dc6f65de238b56145e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56f15439193d229a6e717b8f3cf696fbcece0e2f1eb17dc6f65de238b56145e6->enter($__internal_56f15439193d229a6e717b8f3cf696fbcece0e2f1eb17dc6f65de238b56145e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a94da655a09c40dab323ff31675b57e0fd29477255742101b23e34b1359a26ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a94da655a09c40dab323ff31675b57e0fd29477255742101b23e34b1359a26ef->enter($__internal_a94da655a09c40dab323ff31675b57e0fd29477255742101b23e34b1359a26ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 41
        echo "            ";
        
        $__internal_a94da655a09c40dab323ff31675b57e0fd29477255742101b23e34b1359a26ef->leave($__internal_a94da655a09c40dab323ff31675b57e0fd29477255742101b23e34b1359a26ef_prof);

        
        $__internal_56f15439193d229a6e717b8f3cf696fbcece0e2f1eb17dc6f65de238b56145e6->leave($__internal_56f15439193d229a6e717b8f3cf696fbcece0e2f1eb17dc6f65de238b56145e6_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_21fecfbcf97ec9be4d626f0eb2ce4fab1b5e9aa91aadbfee6a5440fd9e59fc6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21fecfbcf97ec9be4d626f0eb2ce4fab1b5e9aa91aadbfee6a5440fd9e59fc6b->enter($__internal_21fecfbcf97ec9be4d626f0eb2ce4fab1b5e9aa91aadbfee6a5440fd9e59fc6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_fbd9453860bb5a4ee14f25f2764a41f9d39c0ad3042fdf22a7f3546369b88499 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbd9453860bb5a4ee14f25f2764a41f9d39c0ad3042fdf22a7f3546369b88499->enter($__internal_fbd9453860bb5a4ee14f25f2764a41f9d39c0ad3042fdf22a7f3546369b88499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_fbd9453860bb5a4ee14f25f2764a41f9d39c0ad3042fdf22a7f3546369b88499->leave($__internal_fbd9453860bb5a4ee14f25f2764a41f9d39c0ad3042fdf22a7f3546369b88499_prof);

        
        $__internal_21fecfbcf97ec9be4d626f0eb2ce4fab1b5e9aa91aadbfee6a5440fd9e59fc6b->leave($__internal_21fecfbcf97ec9be4d626f0eb2ce4fab1b5e9aa91aadbfee6a5440fd9e59fc6b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 43,  194 => 41,  185 => 40,  174 => 12,  165 => 11,  152 => 6,  143 => 5,  131 => 44,  129 => 43,  126 => 42,  124 => 40,  120 => 38,  117 => 37,  111 => 36,  101 => 32,  95 => 30,  90 => 29,  85 => 28,  83 => 27,  78 => 24,  71 => 22,  64 => 21,  58 => 18,  54 => 17,  49 => 16,  47 => 15,  44 => 14,  42 => 11,  37 => 8,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" type=\"text/css\" rel=\"stylesheet\"/>
        {% endblock %}
    </head>

    <body>
        {% block header %}
            <h1> Desafía a tus compañeros en este TRIVIAL Online </h1>        
        {% endblock %}
        <div>
            {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
                <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
                <a href=\"{{ path('fos_user_registration_register') }}\">{{ 'layout.register'|trans({}, 'FOSUserBundle') }}</a>
            {% endif %}
        </div>


        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        <div class=\"message\">
                            {{ message }}
                        </div>
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "@FOSUser/layout.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/friendsofsymfony/user-bundle/Resources/views/layout.html.twig");
    }
}
