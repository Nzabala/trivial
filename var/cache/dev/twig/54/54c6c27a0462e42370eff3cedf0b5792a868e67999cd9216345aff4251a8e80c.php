<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_cd1104dd9530f9b7e4f13627e36f6c2b000fd4a7b853e76cf69432e5660ad3ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'form' => array($this, 'block_form'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d36ff582e972f9253c234a12eabfd8f38eefaee28b7c75f9e350016e5ef40728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d36ff582e972f9253c234a12eabfd8f38eefaee28b7c75f9e350016e5ef40728->enter($__internal_d36ff582e972f9253c234a12eabfd8f38eefaee28b7c75f9e350016e5ef40728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_99fcf7dde382ae9daa5ad36852ecc81f363fdcf7398af9920a41efbd3463ca07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99fcf7dde382ae9daa5ad36852ecc81f363fdcf7398af9920a41efbd3463ca07->enter($__internal_99fcf7dde382ae9daa5ad36852ecc81f363fdcf7398af9920a41efbd3463ca07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        // line 2
        echo "

";
        // line 4
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_d36ff582e972f9253c234a12eabfd8f38eefaee28b7c75f9e350016e5ef40728->leave($__internal_d36ff582e972f9253c234a12eabfd8f38eefaee28b7c75f9e350016e5ef40728_prof);

        
        $__internal_99fcf7dde382ae9daa5ad36852ecc81f363fdcf7398af9920a41efbd3463ca07->leave($__internal_99fcf7dde382ae9daa5ad36852ecc81f363fdcf7398af9920a41efbd3463ca07_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_47206748f7377e75fb74009a9907145fb2d6c37daf00667bf55833dbbd57a62d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47206748f7377e75fb74009a9907145fb2d6c37daf00667bf55833dbbd57a62d->enter($__internal_47206748f7377e75fb74009a9907145fb2d6c37daf00667bf55833dbbd57a62d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e3942ef9fff0e43f38a0879479244f04a8c8dda312bc75397afce469fbb2dc78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3942ef9fff0e43f38a0879479244f04a8c8dda312bc75397afce469fbb2dc78->enter($__internal_e3942ef9fff0e43f38a0879479244f04a8c8dda312bc75397afce469fbb2dc78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
\t";
        // line 6
        $this->displayBlock('form', $context, $blocks);
        // line 19
        echo "
";
        
        $__internal_e3942ef9fff0e43f38a0879479244f04a8c8dda312bc75397afce469fbb2dc78->leave($__internal_e3942ef9fff0e43f38a0879479244f04a8c8dda312bc75397afce469fbb2dc78_prof);

        
        $__internal_47206748f7377e75fb74009a9907145fb2d6c37daf00667bf55833dbbd57a62d->leave($__internal_47206748f7377e75fb74009a9907145fb2d6c37daf00667bf55833dbbd57a62d_prof);

    }

    // line 6
    public function block_form($context, array $blocks = array())
    {
        $__internal_f21224e4299e08802c4934b712a482171ef3bfba8f0182c4fb13f13e0e7b4497 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f21224e4299e08802c4934b712a482171ef3bfba8f0182c4fb13f13e0e7b4497->enter($__internal_f21224e4299e08802c4934b712a482171ef3bfba8f0182c4fb13f13e0e7b4497_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_7d1a8315178919f55d6b3944a67b63bd8ed02cbeb344fed40ce8e96d35b851d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d1a8315178919f55d6b3944a67b63bd8ed02cbeb344fed40ce8e96d35b851d0->enter($__internal_7d1a8315178919f55d6b3944a67b63bd8ed02cbeb344fed40ce8e96d35b851d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 7
        echo "
\t";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "

\t<div class=\"login-form\">  
\t  <div class=\"field\">
\t  ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo " 
\t</div>
\t<div>
        <button type=\"submit\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
    </div>
\t";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
\t";
        
        $__internal_7d1a8315178919f55d6b3944a67b63bd8ed02cbeb344fed40ce8e96d35b851d0->leave($__internal_7d1a8315178919f55d6b3944a67b63bd8ed02cbeb344fed40ce8e96d35b851d0_prof);

        
        $__internal_f21224e4299e08802c4934b712a482171ef3bfba8f0182c4fb13f13e0e7b4497->leave($__internal_f21224e4299e08802c4934b712a482171ef3bfba8f0182c4fb13f13e0e7b4497_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  95 => 17,  90 => 15,  84 => 12,  77 => 8,  74 => 7,  65 => 6,  54 => 19,  52 => 6,  49 => 5,  31 => 4,  27 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}


{% block body %}

\t{% block form %}

\t{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}

\t<div class=\"login-form\">  
\t  <div class=\"field\">
\t  {{ form_widget(form) }} 
\t</div>
\t<div>
        <button type=\"submit\">{{ 'registration.submit'|trans }}</button>
    </div>
\t{{ form_end(form) }}
\t{% endblock %}

{% endblock %}", "@FOSUser/Registration/register_content.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig");
    }
}
