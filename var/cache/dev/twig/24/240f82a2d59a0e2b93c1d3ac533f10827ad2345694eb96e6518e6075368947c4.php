<?php

/* partidas/content.html.twig */
class __TwigTemplate_f59f01f652db2db744ce36693bc1f63ca808bd1c08af56a2d9ab4c7b555d7a78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "partidas/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'backButton' => array($this, 'block_backButton'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a901721add64f426f184b1769134952f33abd1a13cc3b36c4d96a5263a851575 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a901721add64f426f184b1769134952f33abd1a13cc3b36c4d96a5263a851575->enter($__internal_a901721add64f426f184b1769134952f33abd1a13cc3b36c4d96a5263a851575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partidas/content.html.twig"));

        $__internal_1ea05ae8e5e142f7a91dc7e06600e1c8e7ee05497c2a74247c9d5f5d31086b51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ea05ae8e5e142f7a91dc7e06600e1c8e7ee05497c2a74247c9d5f5d31086b51->enter($__internal_1ea05ae8e5e142f7a91dc7e06600e1c8e7ee05497c2a74247c9d5f5d31086b51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partidas/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a901721add64f426f184b1769134952f33abd1a13cc3b36c4d96a5263a851575->leave($__internal_a901721add64f426f184b1769134952f33abd1a13cc3b36c4d96a5263a851575_prof);

        
        $__internal_1ea05ae8e5e142f7a91dc7e06600e1c8e7ee05497c2a74247c9d5f5d31086b51->leave($__internal_1ea05ae8e5e142f7a91dc7e06600e1c8e7ee05497c2a74247c9d5f5d31086b51_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e9c672280cdb37a9ca70dd3fef827ec41a74cfdf376d6683b35751ba4fff6523 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9c672280cdb37a9ca70dd3fef827ec41a74cfdf376d6683b35751ba4fff6523->enter($__internal_e9c672280cdb37a9ca70dd3fef827ec41a74cfdf376d6683b35751ba4fff6523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8a7c5eb9335cacd02555af0a08eb5a17000357ee5273521151fe8e4d32acc32f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a7c5eb9335cacd02555af0a08eb5a17000357ee5273521151fe8e4d32acc32f->enter($__internal_8a7c5eb9335cacd02555af0a08eb5a17000357ee5273521151fe8e4d32acc32f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> Lista de Partidas </h3>
<table border=\"1\">
    <tr>
        <th>id de partida</th>
        <th>nombre</th>
        <th>Estado</th>
    </tr>
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["partidas"]);
        foreach ($context['_seq'] as $context["_key"] => $context["partidas"]) {
            // line 12
            echo "        <tr>
            ";
            // line 13
            if (($this->getAttribute($context["partidas"], "estado", array()) == "open")) {
                // line 14
                echo "            <td><a href=\"/partida/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "partidaid", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "partidaid", array()), "html", null, true);
                echo "</a></td>
            <td>";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "name", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "estado", array()), "html", null, true);
                echo "</td>
            ";
            } else {
                // line 18
                echo "            <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "partidaid", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "name", array()), "html", null, true);
                echo "</td>
            <td>";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute($context["partidas"], "estado", array()), "html", null, true);
                echo "</td>
            ";
            }
            // line 22
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['partidas'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "</table>
";
        
        $__internal_8a7c5eb9335cacd02555af0a08eb5a17000357ee5273521151fe8e4d32acc32f->leave($__internal_8a7c5eb9335cacd02555af0a08eb5a17000357ee5273521151fe8e4d32acc32f_prof);

        
        $__internal_e9c672280cdb37a9ca70dd3fef827ec41a74cfdf376d6683b35751ba4fff6523->leave($__internal_e9c672280cdb37a9ca70dd3fef827ec41a74cfdf376d6683b35751ba4fff6523_prof);

    }

    // line 26
    public function block_backButton($context, array $blocks = array())
    {
        $__internal_2e0c6bb8a3d67074f3454b052369ad8db42bd9ac42d3e08bb84b617040c5477e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e0c6bb8a3d67074f3454b052369ad8db42bd9ac42d3e08bb84b617040c5477e->enter($__internal_2e0c6bb8a3d67074f3454b052369ad8db42bd9ac42d3e08bb84b617040c5477e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        $__internal_b0dd99b3e3df417428a5250a09c7d96b1654e082eb2f3fc70ee18302ae8defed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0dd99b3e3df417428a5250a09c7d96b1654e082eb2f3fc70ee18302ae8defed->enter($__internal_b0dd99b3e3df417428a5250a09c7d96b1654e082eb2f3fc70ee18302ae8defed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "backButton"));

        // line 27
        echo "    <button class=\"backButton\" type=\"button\" onCLick=\"history.back()\">Atras</button>
";
        
        $__internal_b0dd99b3e3df417428a5250a09c7d96b1654e082eb2f3fc70ee18302ae8defed->leave($__internal_b0dd99b3e3df417428a5250a09c7d96b1654e082eb2f3fc70ee18302ae8defed_prof);

        
        $__internal_2e0c6bb8a3d67074f3454b052369ad8db42bd9ac42d3e08bb84b617040c5477e->leave($__internal_2e0c6bb8a3d67074f3454b052369ad8db42bd9ac42d3e08bb84b617040c5477e_prof);

    }

    public function getTemplateName()
    {
        return "partidas/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 27,  116 => 26,  105 => 24,  98 => 22,  93 => 20,  89 => 19,  84 => 18,  79 => 16,  75 => 15,  68 => 14,  66 => 13,  63 => 12,  59 => 11,  50 => 4,  41 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/partidas/content.html.twig #}
{% extends 'base.html.twig' %}
{% block body %}
    <h3> Lista de Partidas </h3>
<table border=\"1\">
    <tr>
        <th>id de partida</th>
        <th>nombre</th>
        <th>Estado</th>
    </tr>
    {% for partidas in partidas %}
        <tr>
            {% if partidas.estado == \"open\" %}
            <td><a href=\"/partida/{{ partidas.partidaid }}\">{{ partidas.partidaid }}</a></td>
            <td>{{ partidas.name }}</td>
            <td>{{ partidas.estado }}</td>
            {% else %}
            <td>{{ partidas.partidaid }}</td>
            <td>{{ partidas.name }}</td>
            <td>{{ partidas.estado }}</td>
            {% endif %}
        </tr>
    {% endfor %}
</table>
{% endblock %}
{% block backButton %}
    <button class=\"backButton\" type=\"button\" onCLick=\"history.back()\">Atras</button>
{% endblock %}", "partidas/content.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/partidas/content.html.twig");
    }
}
