<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_150ca7d995468e02789c19d70f5a82365301ecebc21b38fde8bc2214e9f04740 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5d33db4d372871d8bba7cece338bd0a3c95f987e9811fb90c40ac82f376f6a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5d33db4d372871d8bba7cece338bd0a3c95f987e9811fb90c40ac82f376f6a4->enter($__internal_e5d33db4d372871d8bba7cece338bd0a3c95f987e9811fb90c40ac82f376f6a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_41b174ee07587a7e5f758ea176a6e5b43867e21edcc8ca78713ac5647cea38cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41b174ee07587a7e5f758ea176a6e5b43867e21edcc8ca78713ac5647cea38cf->enter($__internal_41b174ee07587a7e5f758ea176a6e5b43867e21edcc8ca78713ac5647cea38cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e5d33db4d372871d8bba7cece338bd0a3c95f987e9811fb90c40ac82f376f6a4->leave($__internal_e5d33db4d372871d8bba7cece338bd0a3c95f987e9811fb90c40ac82f376f6a4_prof);

        
        $__internal_41b174ee07587a7e5f758ea176a6e5b43867e21edcc8ca78713ac5647cea38cf->leave($__internal_41b174ee07587a7e5f758ea176a6e5b43867e21edcc8ca78713ac5647cea38cf_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_579b809efdafcec274e8769737d735dcede2bde1013c4d8aa9ced07035b54432 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_579b809efdafcec274e8769737d735dcede2bde1013c4d8aa9ced07035b54432->enter($__internal_579b809efdafcec274e8769737d735dcede2bde1013c4d8aa9ced07035b54432_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_04a3ba618cbd8f641fd8b21af80c416bf77113aa887be2f39289bc8911bd4896 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04a3ba618cbd8f641fd8b21af80c416bf77113aa887be2f39289bc8911bd4896->enter($__internal_04a3ba618cbd8f641fd8b21af80c416bf77113aa887be2f39289bc8911bd4896_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_04a3ba618cbd8f641fd8b21af80c416bf77113aa887be2f39289bc8911bd4896->leave($__internal_04a3ba618cbd8f641fd8b21af80c416bf77113aa887be2f39289bc8911bd4896_prof);

        
        $__internal_579b809efdafcec274e8769737d735dcede2bde1013c4d8aa9ced07035b54432->leave($__internal_579b809efdafcec274e8769737d735dcede2bde1013c4d8aa9ced07035b54432_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_55d7ca8f21a10593a4e5f1e708e012f342d4001f1cc4b23ba8a5991691a7f7b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55d7ca8f21a10593a4e5f1e708e012f342d4001f1cc4b23ba8a5991691a7f7b7->enter($__internal_55d7ca8f21a10593a4e5f1e708e012f342d4001f1cc4b23ba8a5991691a7f7b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cede1ffa09c9b0b322f6fdc5974f3e9ebe5c5df30314e16f91f66be82180af05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cede1ffa09c9b0b322f6fdc5974f3e9ebe5c5df30314e16f91f66be82180af05->enter($__internal_cede1ffa09c9b0b322f6fdc5974f3e9ebe5c5df30314e16f91f66be82180af05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_cede1ffa09c9b0b322f6fdc5974f3e9ebe5c5df30314e16f91f66be82180af05->leave($__internal_cede1ffa09c9b0b322f6fdc5974f3e9ebe5c5df30314e16f91f66be82180af05_prof);

        
        $__internal_55d7ca8f21a10593a4e5f1e708e012f342d4001f1cc4b23ba8a5991691a7f7b7->leave($__internal_55d7ca8f21a10593a4e5f1e708e012f342d4001f1cc4b23ba8a5991691a7f7b7_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
