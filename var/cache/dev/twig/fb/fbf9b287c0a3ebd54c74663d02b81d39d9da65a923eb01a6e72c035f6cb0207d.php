<?php

/* buttonLogin.html.twig */
class __TwigTemplate_38761d6e841ca00504f7595f041deafcb4393895117761c4ad3338f4d345bc2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("message.html.twig", "buttonLogin.html.twig", 2);
        $this->blocks = array(
            'buttonLogin' => array($this, 'block_buttonLogin'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "message.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4492db5e860dcb31583a90dee7ed9908b8f1ccfcb93ed6906de771f2bb615fd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4492db5e860dcb31583a90dee7ed9908b8f1ccfcb93ed6906de771f2bb615fd6->enter($__internal_4492db5e860dcb31583a90dee7ed9908b8f1ccfcb93ed6906de771f2bb615fd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "buttonLogin.html.twig"));

        $__internal_62eb6e79302cdc9a85c2c82f4ed1e666145c51625e7f916266b0ddd70dc0421d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62eb6e79302cdc9a85c2c82f4ed1e666145c51625e7f916266b0ddd70dc0421d->enter($__internal_62eb6e79302cdc9a85c2c82f4ed1e666145c51625e7f916266b0ddd70dc0421d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "buttonLogin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4492db5e860dcb31583a90dee7ed9908b8f1ccfcb93ed6906de771f2bb615fd6->leave($__internal_4492db5e860dcb31583a90dee7ed9908b8f1ccfcb93ed6906de771f2bb615fd6_prof);

        
        $__internal_62eb6e79302cdc9a85c2c82f4ed1e666145c51625e7f916266b0ddd70dc0421d->leave($__internal_62eb6e79302cdc9a85c2c82f4ed1e666145c51625e7f916266b0ddd70dc0421d_prof);

    }

    // line 3
    public function block_buttonLogin($context, array $blocks = array())
    {
        $__internal_693662a4b4f454c254753eb56be6c6cfc6dc115aafc5083d8de1cbd0b5932928 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_693662a4b4f454c254753eb56be6c6cfc6dc115aafc5083d8de1cbd0b5932928->enter($__internal_693662a4b4f454c254753eb56be6c6cfc6dc115aafc5083d8de1cbd0b5932928_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        $__internal_1733947a2510f8377fb0c67ab67e9dd3984a04b2c7075a6df389a26567098e27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1733947a2510f8377fb0c67ab67e9dd3984a04b2c7075a6df389a26567098e27->enter($__internal_1733947a2510f8377fb0c67ab67e9dd3984a04b2c7075a6df389a26567098e27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "buttonLogin"));

        // line 4
        echo "<div class=\"message2\">
\t<div class=\"botones\">    
\t\t<button type=\"button\" onclick=\"window.location.href='/login'\">Entrar</button>
    \t<button type=\"button\" onclick=\"window.location.href='/register'\">Registrate</button>
    </div>
</div>
";
        
        $__internal_1733947a2510f8377fb0c67ab67e9dd3984a04b2c7075a6df389a26567098e27->leave($__internal_1733947a2510f8377fb0c67ab67e9dd3984a04b2c7075a6df389a26567098e27_prof);

        
        $__internal_693662a4b4f454c254753eb56be6c6cfc6dc115aafc5083d8de1cbd0b5932928->leave($__internal_693662a4b4f454c254753eb56be6c6cfc6dc115aafc5083d8de1cbd0b5932928_prof);

    }

    public function getTemplateName()
    {
        return "buttonLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/buttonLogin.html.twig #}
{% extends 'message.html.twig' %}
{% block buttonLogin %}
<div class=\"message2\">
\t<div class=\"botones\">    
\t\t<button type=\"button\" onclick=\"window.location.href='/login'\">Entrar</button>
    \t<button type=\"button\" onclick=\"window.location.href='/register'\">Registrate</button>
    </div>
</div>
{% endblock %}", "buttonLogin.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/app/Resources/views/buttonLogin.html.twig");
    }
}
