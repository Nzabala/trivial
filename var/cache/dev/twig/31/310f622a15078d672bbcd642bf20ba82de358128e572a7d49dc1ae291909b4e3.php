<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_3bbb6c6803d3f6a382be84877d6b3883a0ab65186a35fac3f7b35f8c4c47b064 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc826acf96ed964ffd8abe5aabd6f4fc05b34868c39b6171b5136ba7e91cecb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc826acf96ed964ffd8abe5aabd6f4fc05b34868c39b6171b5136ba7e91cecb4->enter($__internal_dc826acf96ed964ffd8abe5aabd6f4fc05b34868c39b6171b5136ba7e91cecb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_ed6414c15412423210b5512a42a2d9e26060e9851013f073929c11756fedb78d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed6414c15412423210b5512a42a2d9e26060e9851013f073929c11756fedb78d->enter($__internal_ed6414c15412423210b5512a42a2d9e26060e9851013f073929c11756fedb78d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dc826acf96ed964ffd8abe5aabd6f4fc05b34868c39b6171b5136ba7e91cecb4->leave($__internal_dc826acf96ed964ffd8abe5aabd6f4fc05b34868c39b6171b5136ba7e91cecb4_prof);

        
        $__internal_ed6414c15412423210b5512a42a2d9e26060e9851013f073929c11756fedb78d->leave($__internal_ed6414c15412423210b5512a42a2d9e26060e9851013f073929c11756fedb78d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b8e77fe1962ef48e4e794b3eca61a2002dbb761105972e9537e8577e9030d42b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8e77fe1962ef48e4e794b3eca61a2002dbb761105972e9537e8577e9030d42b->enter($__internal_b8e77fe1962ef48e4e794b3eca61a2002dbb761105972e9537e8577e9030d42b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ebed2df35e9b34cc19c35bd25380df558fffb7054c95cbd3928dfaf8bc2dc0e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebed2df35e9b34cc19c35bd25380df558fffb7054c95cbd3928dfaf8bc2dc0e8->enter($__internal_ebed2df35e9b34cc19c35bd25380df558fffb7054c95cbd3928dfaf8bc2dc0e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_ebed2df35e9b34cc19c35bd25380df558fffb7054c95cbd3928dfaf8bc2dc0e8->leave($__internal_ebed2df35e9b34cc19c35bd25380df558fffb7054c95cbd3928dfaf8bc2dc0e8_prof);

        
        $__internal_b8e77fe1962ef48e4e794b3eca61a2002dbb761105972e9537e8577e9030d42b->leave($__internal_b8e77fe1962ef48e4e794b3eca61a2002dbb761105972e9537e8577e9030d42b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
