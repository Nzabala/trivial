<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_890b67787fcc83fc9d784615f1ae26eed23791a6cc23607d212b422c1fdcfcef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8502e30ec7700a16b06a61813387ae6bc4a271451503cfd47171d78df5b7a31c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8502e30ec7700a16b06a61813387ae6bc4a271451503cfd47171d78df5b7a31c->enter($__internal_8502e30ec7700a16b06a61813387ae6bc4a271451503cfd47171d78df5b7a31c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_f8dc5bdf0bbfb9ba6e2e1d7506e32457c833259f31db84d02f84458ac8181de5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8dc5bdf0bbfb9ba6e2e1d7506e32457c833259f31db84d02f84458ac8181de5->enter($__internal_f8dc5bdf0bbfb9ba6e2e1d7506e32457c833259f31db84d02f84458ac8181de5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 4
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo "<div class=\"login-form\">  
    <form action=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
        ";
        // line 8
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 9
            echo "            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
        ";
        }
        // line 11
        echo "        
        <div class=\"field\">
            <label for=\"username\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" /><br>     

            <label for=\"password\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" /><br>


            <label for=\"remember_me\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
            <button type=\"submit\" id=\"_submit\" name=\"_submit\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
        </div>
    </form>
</div>
";
        // line 26
        $this->displayBlock('javascripts', $context, $blocks);
        
        $__internal_8502e30ec7700a16b06a61813387ae6bc4a271451503cfd47171d78df5b7a31c->leave($__internal_8502e30ec7700a16b06a61813387ae6bc4a271451503cfd47171d78df5b7a31c_prof);

        
        $__internal_f8dc5bdf0bbfb9ba6e2e1d7506e32457c833259f31db84d02f84458ac8181de5->leave($__internal_f8dc5bdf0bbfb9ba6e2e1d7506e32457c833259f31db84d02f84458ac8181de5_prof);

    }

    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_dbd7277c3ed54eee8ca6992df2adffc7f91fe5240bd45acd152fe2c9252b2806 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbd7277c3ed54eee8ca6992df2adffc7f91fe5240bd45acd152fe2c9252b2806->enter($__internal_dbd7277c3ed54eee8ca6992df2adffc7f91fe5240bd45acd152fe2c9252b2806_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_f618d87423a2417d2b1a222665ea90af32cfd816c944f82bc64e8767ec556ab9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f618d87423a2417d2b1a222665ea90af32cfd816c944f82bc64e8767ec556ab9->enter($__internal_f618d87423a2417d2b1a222665ea90af32cfd816c944f82bc64e8767ec556ab9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 27
        echo "    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js\"></script>
    <script src=\"http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
";
        
        $__internal_f618d87423a2417d2b1a222665ea90af32cfd816c944f82bc64e8767ec556ab9->leave($__internal_f618d87423a2417d2b1a222665ea90af32cfd816c944f82bc64e8767ec556ab9_prof);

        
        $__internal_dbd7277c3ed54eee8ca6992df2adffc7f91fe5240bd45acd152fe2c9252b2806->leave($__internal_dbd7277c3ed54eee8ca6992df2adffc7f91fe5240bd45acd152fe2c9252b2806_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 27,  84 => 26,  77 => 22,  72 => 20,  65 => 16,  60 => 14,  56 => 13,  52 => 11,  46 => 9,  44 => 8,  40 => 7,  37 => 6,  31 => 4,  29 => 3,  26 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
<div class=\"login-form\">  
    <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
        {% if csrf_token %}
            <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
        {% endif %}
        
        <div class=\"field\">
            <label for=\"username\">{{ 'security.login.username'|trans }}</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" /><br>     

            <label for=\"password\">{{ 'security.login.password'|trans }}</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" /><br>


            <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>
            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
            <button type=\"submit\" id=\"_submit\" name=\"_submit\">{{ 'security.login.submit'|trans }}</button>
        </div>
    </form>
</div>
{% block javascripts %}
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js\"></script>
    <script src=\"http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
{% endblock %}
", "@FOSUser/Security/login_content.html.twig", "/home/ausias/Escriptori/proyectos-symfony/trivial/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login_content.html.twig");
    }
}
