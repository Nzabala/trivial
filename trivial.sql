CREATE DATABASE dbTrivial;
use dbTrivial;
CREATE TABLE jugador (
	playerid int NOT NULL AUTO_INCREMENT,
	name VARCHAR(10) NOT NULL,
	score INT,
	wins INT,
	PRIMARY KEY (playerid)
);
CREATE TABLE partida (
	partidaid int NOT NULL AUTO_INCREMENT,
	estado VARCHAR(10) NOT NULL,
	PRIMARY KEY (partidaid)
);

CREATE TABLE quesera (
	playerid int,
	partidaid int,
	posicionX int,
	posicionY int,
	quesitoA boolean,
	quesitoR boolean,
	quesitoV boolean,
	quesitoB boolean,
	quesitoL boolean,
	quesitoF boolean,
	PRIMARY KEY (playerid,partidaid),
	FOREIGN KEY (playerid) REFERENCES jugador (playerid),
	FOREIGN KEY (partidaid) REFERENCES partida (partidaid)
);




