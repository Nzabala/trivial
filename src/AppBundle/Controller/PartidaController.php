<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Partida;
use AppBundle\Entity\Jugador;
use AppBundle\Entity\Quesera;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PartidaController extends Controller
{
    /**
     * @Route("/showGames", name="showGames")
     */
    public function showGamesAction(Request $request)
    {
    	$partidas = $this->getDoctrine()
            ->getRepository('AppBundle:Partida')
            ->findAll();
        if($partidas){
            return $this->render('partidas/content.html.twig',array('partidas' => $partidas));    
        }else{
           return $this->render('message.html.twig', array(
                   'message' => 'No hay partidas',
                   'message2' => '¿Deseas crear una nueva Partida?'));
        }
    }

    /**
     * @Route("/partida/{partidaid}", name="partida")
     */
    public function jugarPartidaAction($partidaid,Request $request)
    {
    	$em = $this->getDoctrine()->getManager();

        $jugador = $em->getRepository('AppBundle:Jugador')
            ->findOneByName($this->getUser()->getUsername());

        $partida = $em->getRepository('AppBundle:Partida')
            ->findOneByPartidaid($partidaid);

        $quesera = $em->getRepository('AppBundle:Quesera')
                      ->findBy(array('partidaid' => $partidaid, 'playerid'=>$jugador->getPlayerid()));

        if(!$jugador){

            $jugador = new Jugador();
            $jugador->setName($this->getUser()->getUsername());
            $jugador->setScore(0);
            $jugador->setWins(0);

            $em->persist($jugador);
            $em->flush();
        }
        if(!$quesera){

            $quesera = new Quesera();
            $quesera->setPosicionX('400');
            $quesera->setPosicionY('400');
            $quesera->setQuesitoa('0');
            $quesera->setQuesitob('0');
            $quesera->setQuesitor('0');
            $quesera->setQuesitof('0');
            $quesera->setQuesitov('0');
            $quesera->setQuesitol('0');
            $quesera->setPartidaid($partida);
            $quesera->setPlayerid($jugador);

            $em->persist($quesera);
            $em->flush();
        }
        
        return $this->render('tablero.html.twig');
    }

    /**
     * @Route("/newGame", name="newGame")
     */
    public function newGameAction(Request $request)
    {

        $partida = new Partida();

        $form = $this->createFormBuilder($partida)
            ->add('estado', HiddenType::class, array(
                'data' => 'open'))
            ->add('time', NumberType::class, array(
                'attr' => array('min' => 1, 'max' => 60)))
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Jugar'))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($partida);
            $em->flush();
            return $this->render('message.html.twig', array(
                'message' => 'Partida creada correctamente!',
                'message2' => 'GL and HF',
                'idPartida' => $partida->getPartidaid()
            ));
        }

        return $this->render('partidas/form.html.twig', array(
            'title' => 'Creando partida ',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/deleteGame", name="deleteGame")
     */
    public function deleteGameAction(Request $request)
    {
        $partida = new Partida();

        $form = $this->createFormBuilder($partida)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Eliminar'))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $partida = $em->getRepository('AppBundle:Partida')
                      ->findOneByName($partida->getName());

            $jugador = $em->getRepository('AppBundle:Jugador')
                        ->findOneByName($this->getUser()->getUsername());

            $quesera = $em->getRepository('AppBundle:Quesera')
                        ->findBy(array('partidaid' => $partida->getPartidaid(), 'playerid'=>$jugador->getPlayerid()));
    
            if (!$partida) {
                return $this->render('message.html.twig', array(
                    'message' => 'No hay partidas con ese id ',
                    'message2' => 'Sorry!'
                    ));
            }
            $em->remove($quesera[0]);
            $em->remove($partida);
            $em->flush();
            return $this->render('message.html.twig', array(
                'message' => 'Partida Eliminada correctamente!',
                'message2' => 'Gratz!'
            ));
        }

        return $this->render('partidas/form.html.twig', array(
            'title' => 'Eliminando partida ',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/searchGame", name="searchGame")
     */
    public function searchGameAction(Request $request)
    {
        $partidas = $this->getDoctrine()
            ->getRepository('AppBundle:Partida')
            ->findAll();
        if (count($partidas)==0) {
            return $this->render('message.html.twig', array(
                'message' => 'No Partidas found',
                'message2' => null));
        }
        return $this->render('partidas/content.html.twig', array(
            'partidas' => $partidas)); 
    }
}