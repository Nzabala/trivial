<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class JugadorController extends Controller
{
    /**
     * @Route("/user", name="user")
     */
    public function indexUserAction(Request $request)
    {
        return new Response("<h1>Página de usuario</h1>");
    }
    /**
     * @Route("/newUser", name="newUser")
     */
    public function newUserAction(Request $request)
    {
        return new Response("<h1>Nuevo usuario</h1>");
    }
    /**
     * @Route("/editUser", name="editUser")
     */
    public function editUserAction(Request $request)
    {
        return new Response("<h1>Editar usuario</h1>");
    }
    /**
     * @Route("/showUser", name="showUser")
     */
    public function showUserAction(Request $request)
    {
        return new Response("<h1>Seleccionar usuario</h1>");
    }

    /**
     * @Route("/deleteUser", name="deleteUser")
     */
    public function deleteUserAction(Request $request)
    {
        return new Response("<h1>Eliminar usuario</h1>");
    }

     /**
     * @Route("/showAllPlayers", name="showAllPlayers")
     */
    public function showAllPlayersAction()
    {
	 	$jugadores = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador')
            ->findAll();
        if (count($jugadores)==0) {
            return $this->render('message.html.twig', array(
                'message' => 'No Jugadores found',
                'message2' => null));
        }
        return $this->render('jugadores/content.html.twig', array(
            'jugadores' => $jugadores));    
      
    }  
}
