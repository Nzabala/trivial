<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use AppBundle\Entity\Partida;
use AppBundle\Entity\Pregunta;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
    	$user = $this->getUser();
    	
    	if (!$user){

        return $this->render('buttonLogin.html.twig', array(
            'title' => 'Trivial Online',
            'message' => '¿Estas preparado para el reto?',
            'message2' => 'Entra ahora o registrate'));
    	}else return $this->redirectToRoute('hall');
    	
    }

    /**
     * @Route("/hall", name="hall")
     */
    public function hallAction(Request $request)
    {
        return $this->render('default/index.html.twig',array(
        'message2' => null, 'form' => null));
    }



}
