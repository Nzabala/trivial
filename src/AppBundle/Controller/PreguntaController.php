<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pregunta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PreguntaController extends Controller
{
    /**
     * @Route("/newQuiz", name="newQuiz")
     */
    public function newQuizAction(Request $request)
    {
        return new Response("<h1>Página de preguntas</h1>");
    }
    
    /**
     * @Route("/editQuiz", name="editQuiz")
     */
    public function editQuizAction(Request $request)
    {
        return new Response("<h1>Editar pregunta</h1>");
    }
	 
	 /**
     * @Route("/removeQuiz", name="removeQuiz")
     */
    public function removeQuizAction(Request $request)
    {
        return new Response("<h1>Borrar pregunta</h1>");
    }    
 	 
 	 /**
     * @Route("/showQuiz", name="showQuiz")
     */
    public function showQuizAction(Request $request)
    {
        return new Response("<h1>Mostrar pregunta</h1>");
    } 
    
    /**
     * @Route("/showAllQuiz", name="showAllQuiz")
     */
    public function showAllQuizAction()
    {
	 	  $preguntas = $this->getDoctrine()
            ->getRepository('AppBundle:Pregunta')
            ->findAll();
        if (count($preguntas)==0) {
            return $this->render('preguntas/content.html.twig', array(
                'message' => 'No Preguntas found'));
        }
        return $this->render('preguntas/content.html.twig', array(
            'preguntas' => $preguntas));    
      
    }       
}