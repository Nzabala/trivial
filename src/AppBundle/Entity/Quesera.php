<?php

// src/AppBundle/Entity/Quesera.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="quesera")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\QueseraRepository")
 */

class Quesera
{	
    /**
     * Many Quesera have One Jugador.
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="jugador", inversedBy="quesera")
     * @ORM\JoinColumn(name="playerid", referencedColumnName="playerid")
     */
    private $playerid;

    /**
     * Many Quesera have One Partida.
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="partida", inversedBy="quesera")
     * @ORM\JoinColumn(name="partidaid", referencedColumnName="partidaid")
     */
    private $partidaid;

    /**
     * @ORM\Column(type="integer")
     */
    protected $posicionX;

    /**
     * @ORM\Column(type="integer")
     */
    protected $posicionY;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoA;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoR;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoV;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoB;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoL;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $quesitoF;


  

    /**
     * Set posicionX
     *
     * @param integer $posicionX
     *
     * @return Quesera
     */
    public function setPosicionX($posicionX)
    {
        $this->posicionX = $posicionX;

        return $this;
    }

    /**
     * Get posicionX
     *
     * @return integer
     */
    public function getPosicionX()
    {
        return $this->posicionX;
    }

    /**
     * Set posicionY
     *
     * @param integer $posicionY
     *
     * @return Quesera
     */
    public function setPosicionY($posicionY)
    {
        $this->posicionY = $posicionY;

        return $this;
    }

    /**
     * Get posicionY
     *
     * @return integer
     */
    public function getPosicionY()
    {
        return $this->posicionY;
    }

    /**
     * Set quesitoA
     *
     * @param boolean $quesitoA
     *
     * @return Quesera
     */
    public function setQuesitoA($quesitoA)
    {
        $this->quesitoA = $quesitoA;

        return $this;
    }

    /**
     * Get quesitoA
     *
     * @return boolean
     */
    public function getQuesitoA()
    {
        return $this->quesitoA;
    }

    /**
     * Set quesitoR
     *
     * @param boolean $quesitoR
     *
     * @return Quesera
     */
    public function setQuesitoR($quesitoR)
    {
        $this->quesitoR = $quesitoR;

        return $this;
    }

    /**
     * Get quesitoR
     *
     * @return boolean
     */
    public function getQuesitoR()
    {
        return $this->quesitoR;
    }

    /**
     * Set quesitoV
     *
     * @param boolean $quesitoV
     *
     * @return Quesera
     */
    public function setQuesitoV($quesitoV)
    {
        $this->quesitoV = $quesitoV;

        return $this;
    }

    /**
     * Get quesitoV
     *
     * @return boolean
     */
    public function getQuesitoV()
    {
        return $this->quesitoV;
    }

    /**
     * Set quesitoB
     *
     * @param boolean $quesitoB
     *
     * @return Quesera
     */
    public function setQuesitoB($quesitoB)
    {
        $this->quesitoB = $quesitoB;

        return $this;
    }

    /**
     * Get quesitoB
     *
     * @return boolean
     */
    public function getQuesitoB()
    {
        return $this->quesitoB;
    }

    /**
     * Set quesitoL
     *
     * @param boolean $quesitoL
     *
     * @return Quesera
     */
    public function setQuesitoL($quesitoL)
    {
        $this->quesitoL = $quesitoL;

        return $this;
    }

    /**
     * Get quesitoL
     *
     * @return boolean
     */
    public function getQuesitoL()
    {
        return $this->quesitoL;
    }

    /**
     * Set quesitoF
     *
     * @param boolean $quesitoF
     *
     * @return Quesera
     */
    public function setQuesitoF($quesitoF)
    {
        $this->quesitoF = $quesitoF;

        return $this;
    }

    /**
     * Get quesitoF
     *
     * @return boolean
     */
    public function getQuesitoF()
    {
        return $this->quesitoF;
    }

    /**
     * Set playerid
     *
     * @param \AppBundle\Entity\jugador $playerid
     *
     * @return Quesera
     */
    public function setPlayerid(\AppBundle\Entity\jugador $playerid)
    {
        $this->playerid = $playerid;

        return $this;
    }

    /**
     * Get playerid
     *
     * @return \AppBundle\Entity\jugador
     */
    public function getPlayerid()
    {
        return $this->playerid;
    }

    /**
     * Set partidaid
     *
     * @param \AppBundle\Entity\partida $partidaid
     *
     * @return Quesera
     */
    public function setPartidaid(\AppBundle\Entity\partida $partidaid)
    {
        $this->partidaid = $partidaid;

        return $this;
    }

    /**
     * Get partidaid
     *
     * @return \AppBundle\Entity\partida
     */
    public function getPartidaid()
    {
        return $this->partidaid;
    }
}
