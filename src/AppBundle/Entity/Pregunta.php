<?php

// src/AppBundle/Entity/Pregunta.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="preguntas")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PreguntaRepository")
 */

class Pregunta
{	
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $quizid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $question;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $answer1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $answer2;
		
	 /**
     * @ORM\Column(type="string", length=255)
     */
    protected $answer3;
    	    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $answer4;

    /**
     * Get quizId
     *
     * @return integer
     */
    public function getQuizId()
    {
        return $this->quizid;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Pregunta
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer1
     *
     * @param string $answer1
     *
     * @return Pregunta
     */
    public function setAnswer1($answer1)
    {
        $this->answer1 = $answer1;

        return $this;
    }

    /**
     * Get answer1
     *
     * @return string
     */
    public function getAnswer1()
    {
        return $this->answer1;
    }

    /**
     * Set answer2
     *
     * @param string $answer2
     *
     * @return Pregunta
     */
    public function setAnswer2($answer2)
    {
        $this->answer2 = $answer2;

        return $this;
    }

    /**
     * Get answer2
     *
     * @return string
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Set answer3
     *
     * @param string $answer3
     *
     * @return Pregunta
     */
    public function setAnswer3($answer3)
    {
        $this->answer3 = $answer3;

        return $this;
    }

    /**
     * Get answer3
     *
     * @return string
     */
    public function getAnswer3()
    {
        return $this->answer3;
    }

    /**
     * Set answer4
     *
     * @param string $answer4
     *
     * @return Pregunta
     */
    public function setAnswer4($answer4)
    {
        $this->answer4 = $answer4;

        return $this;
    }

    /**
     * Get answer4
     *
     * @return string
     */
    public function getAnswer4()
    {
        return $this->answer4;
    }
}
