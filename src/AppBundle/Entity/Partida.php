<?php

// src/AppBundle/Entity/Partida.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="partida")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\JugadorRepository")
 */

class Partida
{	
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $partidaid;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $estado;

    /**
     * @ORM\Column(type="integer")
     */
    protected $time;
    
    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $name;
    /**
     * Get partidaid
     *
     * @return integer
     */
    public function getPartidaid()
    {
        return $this->partidaid;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Partida
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return Partida
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partida
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
